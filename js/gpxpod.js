(function ($, OC) {
'use strict';

var colors = [ 'red', 'cyan', 'purple','Lime', 'yellow',
               'orange', 'blue', 'brown', 'Chartreuse','Crimson',
               'DeepPink', 'Gold'];
var lastColorUsed = -1;
var gpxpod = {
    map: {},
    baseLayers: null,
    markers: [],
    markersPopupTxt: {},
    markerLayer: null,
    // layers currently displayed, indexed by track name
    gpxlayers: {},
    gpxCache: {},
    subfolder: '',
    // layer of current elevation chart
    elevationLayer: null,
    // track concerned by elevation
    elevationTrack: null,
    minimapControl: null,
    searchControl: null,
    tablesortCol: [2,1],
    currentHoverLayer : null,
    currentHoverLayerOutlines : L.layerGroup(),
    currentAjax : null,
    currentMarkerAjax : null,
    // as tracks are retrieved by ajax, there's a lapse between mousein event
    // on table rows and track overview display, if mouseout was triggered
    // during this lapse, track was displayed anyway. i solve it by keeping
    // this prop up to date and drawing ajax result just if its value is true
    insideTr: false,
    picturePopups: [],
    pictureSmallMarkers: [],
    pictureBigMarkers: []
};

var hoverStyle = {
    weight: 12,
    opacity: 0.7,
    color: 'black'
};
var defaultStyle = {
    weight: 5,
    opacity: 0.9
};

/*
 * markers are stored as list of values in this format :
 *
 * m[0] : lat,
 * m[1] : lon,
 * m[2] : name,
 * m[3] : total_distance,
 * m[4] : total_duration,
 * m[5] : date_begin,
 * m[6] : date_end,
 * m[7] : pos_elevation,
 * m[8] : neg_elevation,
 * m[9] : min_elevation,
 * m[10] : max_elevation,
 * m[11] : max_speed,
 * m[12] : avg_speed
 * m[13] : moving_time
 * m[14] : stopped_time
 * m[15] : moving_avg_speed
 * m[16] : north
 * m[17] : south
 * m[18] : east
 * m[19] : west
 * m[20] : shortPointList
 *
 */

var LAT = 0;
var LON = 1;
var NAME = 2;
var TOTAL_DISTANCE = 3;
var TOTAL_DURATION = 4;
var DATE_BEGIN = 5;
var DATE_END = 6;
var POSITIVE_ELEVATION_GAIN = 7;
var NEGATIVE_ELEVATION_GAIN = 8;
var MIN_ELEVATION = 9;
var MAX_ELEVATION = 10;
var MAX_SPEED = 11;
var AVERAGE_SPEED = 12;
var MOVING_TIME = 13;
var STOPPED_TIME = 14;
var MOVING_AVERAGE_SPEED = 15;
var NORTH = 16;
var SOUTH = 17;
var EAST = 18;
var WEST = 19;
var SHORTPOINTLIST = 20;
var TRACKNAMELIST = 21;

var symbolSelectClasses = {
    'Dot, White': 'dot-select',
    'Pin, Blue': 'pin-blue-select',
    'Pin, Green': 'pin-green-select',
    'Pin, Red': 'pin-red-select',
    'Flag, Green': 'flag-green-select',
    'Flag, Red': 'flag-red-select',
    'Flag, Blue': 'flag-blue-select',
    'Block, Blue': 'block-blue-select',
    'Block, Green': 'block-green-select',
    'Block, Red': 'block-red-select',
    'Blue Diamond': 'diamond-blue-select',
    'Green Diamond': 'diamond-green-select',
    'Red Diamond': 'diamond-red-select',
    'Residence': 'residence-select',
    'Drinking Water': 'drinking-water-select',
    'Trail Head': 'hike-select',
    'Bike Trail': 'bike-trail-select',
    'Campground': 'campground-select',
    'Bar': 'bar-select',
    'Skull and Crossbones': 'skullcross-select',
    'Geocache': 'geocache-select',
    'Geocache Found': 'geocache-open-select',
    'Medical Facility': 'medical-select',
    'Contact, Alien': 'contact-alien-select',
    'Contact, Big Ears': 'contact-bigears-select',
    'Contact, Female3': 'contact-female3-select',
    'Contact, Cat': 'contact-cat-select',
    'Contact, Dog': 'contact-dog-select',
}

var symbolIcons = {
    'Dot, White': L.divIcon({
            iconSize:L.point(7,7),
    }),
    'Pin, Blue': L.divIcon({
        className: 'pin-blue',
        iconAnchor: [5, 30]
    }),
    'Pin, Green': L.divIcon({
        className: 'pin-green',
        iconAnchor: [5, 30]
    }),
    'Pin, Red': L.divIcon({
        className: 'pin-red',
        iconAnchor: [5, 30]
    }),
    'Flag, Green': L.divIcon({
        className: 'flag-green',
        iconAnchor: [1, 25]
    }),
    'Flag, Red': L.divIcon({
        className: 'flag-red',
        iconAnchor: [1, 25]
    }),
    'Flag, Blue': L.divIcon({
        className: 'flag-blue',
        iconAnchor: [1, 25]
    }),
    'Block, Blue': L.divIcon({
        className: 'block-blue',
        iconAnchor: [8, 8]
    }),
    'Block, Green': L.divIcon({
        className: 'block-green',
        iconAnchor: [8, 8]
    }),
    'Block, Red': L.divIcon({
        className: 'block-red',
        iconAnchor: [8, 8]
    }),
    'Blue Diamond': L.divIcon({
        className: 'diamond-blue',
        iconAnchor: [9, 9]
    }),
    'Green Diamond': L.divIcon({
        className: 'diamond-green',
        iconAnchor: [9, 9]
    }),
    'Red Diamond': L.divIcon({
        className: 'diamond-red',
        iconAnchor: [9, 9]
    }),
    'Residence': L.divIcon({
        className: 'residence',
        iconAnchor: [12, 12]
    }),
    'Drinking Water': L.divIcon({
        className: 'drinking-water',
        iconAnchor: [12, 12]
    }),
    'Trail Head': L.divIcon({
        className: 'hike',
        iconAnchor: [12, 12]
    }),
    'Bike Trail': L.divIcon({
        className: 'bike-trail',
        iconAnchor: [12, 12]
    }),
    'Campground': L.divIcon({
        className: 'campground',
        iconAnchor: [12, 12]
    }),
    'Bar': L.divIcon({
        className: 'bar',
        iconAnchor: [10, 12]
    }),
    'Skull and Crossbones': L.divIcon({
        className: 'skullcross',
        iconAnchor: [12, 12]
    }),
    'Geocache': L.divIcon({
        className: 'geocache',
        iconAnchor: [11, 10]
    }),
    'Geocache Found': L.divIcon({
        className: 'geocache-open',
        iconAnchor: [11, 10]
    }),
    'Medical Facility': L.divIcon({
        className: 'medical',
        iconAnchor: [13, 11]
    }),
    'Contact, Alien': L.divIcon({
        className: 'contact-alien',
        iconAnchor: [12, 12]
    }),
    'Contact, Big Ears': L.divIcon({
        className: 'contact-bigears',
        iconAnchor: [12, 12]
    }),
    'Contact, Female3': L.divIcon({
        className: 'contact-female3',
        iconAnchor: [12, 12]
    }),
    'Contact, Cat': L.divIcon({
        className: 'contact-cat',
        iconAnchor: [12, 12]
    }),
    'Contact, Dog': L.divIcon({
        className: 'contact-dog',
        iconAnchor: [12, 12]
    }),
}

function load()
{
    load_map();
}

function load_map() {
  // change meta to send referrer
  // usefull for IGN tiles authentication !
  $('meta[name=referrer]').attr('content', 'origin');

  var layer = getUrlParameter('layer');
  console.log('layer '+layer);
  var default_layer = 'OpenStreetMap';
  if (typeof layer !== 'undefined'){
      default_layer = decodeURI(layer);
  }

  // get url from key and layer type
  function geopUrl (key, layer, format)
  { return 'http://wxs.ign.fr/'+ key + '/wmts?LAYER=' + layer
      +'&EXCEPTIONS=text/xml&FORMAT='+(format?format:'image/jpeg')
          +'&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&STYLE=normal'
          +'&TILEMATRIXSET=PM&TILEMATRIX={z}&TILECOL={x}&TILEROW={y}' ;
  }
  // change it if you deploy GPXPOD
  var API_KEY = 'ljthe66m795pr2v2g8p7faxt';
  var ign = new L.tileLayer ( geopUrl(API_KEY,'GEOGRAPHICALGRIDSYSTEMS.MAPS'),
          { attribution:'&copy; <a href="http://www.ign.fr/">IGN-France</a>',
              maxZoom:18
          });

  var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  var osmAttribution = 'Map data &copy; 2013 <a href="http://openstreetmap'+
                       '.org">OpenStreetMap</a> contributors';
  var osm = new L.TileLayer(osmUrl, {maxZoom: 18, attribution: osmAttribution});

  var osmfrUrl = 'http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
  var osmfr = new L.TileLayer(osmfrUrl,
              {maxZoom: 20, attribution: osmAttribution});
  var osmfr2 = new L.TileLayer(osmfrUrl,
               {minZoom: 0, maxZoom: 13, attribution: osmAttribution});

  var openmapsurferUrl = 'http://openmapsurfer.uni-hd.de/tiles/roads/'+
                         'x={x}&y={y}&z={z}';
  var openmapsurferAttribution = 'Imagery from <a href="http://giscience.uni'+
  '-hd.de/">GIScience Research Group @ University of Heidelberg</a> &mdash; '+
  'Map data &copy; <a href="http://www.openstreetmap.org/copyright">'+
  'OpenStreetMap</a>';
  var openmapsurfer = new L.TileLayer(openmapsurferUrl,
                      {maxZoom: 18, attribution: openmapsurferAttribution});

  var transportUrl = 'http://a.tile2.opencyclemap.org/transport/{z}/{x}/{y}.'+
                     'png';
  var transport = new L.TileLayer(transportUrl,
                  {maxZoom: 18, attribution: osmAttribution});

  var pisteUrl = 'http://tiles.openpistemap.org/nocontours/{z}/{x}/{y}.png';
  var piste = new L.TileLayer(pisteUrl,
              {maxZoom: 18, attribution: osmAttribution});

  var hikebikeUrl = 'http://toolserver.org/tiles/hikebike/{z}/{x}/{y}.png';
  var hikebike = new L.TileLayer(hikebikeUrl,
                 {maxZoom: 18, attribution: osmAttribution});

  var osmCycleUrl = 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png';
  var osmCycleAttrib = '&copy; <a href="http://www.opencyclemap.org">'+
  'OpenCycleMap</a>, &copy; <a href="http://www.openstreetmap.org/copyright">'+
  'OpenStreetMap</a>';
  var osmCycle = new L.TileLayer(osmCycleUrl,
                 {maxZoom: 18, attribution: osmCycleAttrib});

  var darkUrl = 'http://a.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png';
  var darkAttrib = '&copy; Map tiles by CartoDB, under CC BY 3.0. Data by'+
                   ' OpenStreetMap, under ODbL.';
  var dark = new L.TileLayer(darkUrl, {maxZoom: 18, attribution: darkAttrib});

  var esriTopoUrl = 'https://server.arcgisonline.com/ArcGIS/rest/services/World'+
                    '_Topo_Map/MapServer/tile/{z}/{y}/{x}';
  var esriTopoAttrib = 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, '+
  'TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ord'+
  'nance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User'+
  ' Community';
  var esriTopo = new L.TileLayer(esriTopoUrl,
                 {maxZoom: 18, attribution: esriTopoAttrib});

  var esriAerialUrl = 'https://server.arcgisonline.com/ArcGIS/rest/services'+
                      '/World_Imagery/MapServer/tile/{z}/{y}/{x}';
  var esriAerialAttrib = 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, '+
  'USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the'+
  ' GIS User Community';
  var esriAerial = new L.TileLayer(esriAerialUrl,
                   {maxZoom: 18, attribution: esriAerialAttrib});

  var tonerUrl = 'http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.jpg';
  var stamenAttribution = '<a href="http://leafletjs.com" title="A JS library'+
  ' for interactive maps">Leaflet</a> | © Map tiles by <a href="http://stamen'+
  '.com">Stamen Design</a>, under <a href="http://creativecommons.org/license'+
  's/by/3.0">CC BY 3.0</a>, Data by <a href="http://openstreetmap.org">OpenSt'+
  'reetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0"'+
  '>CC BY SA</a>.';
  var toner = new L.TileLayer(tonerUrl,
              {maxZoom: 18, attribution: stamenAttribution});

  var watercolorUrl = 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg';
  var watercolor = new L.TileLayer(watercolorUrl,
                   {maxZoom: 18, attribution: stamenAttribution});

  var routeUrl = 'http://{s}.tile.openstreetmap.fr/route500/{z}/{x}/{y}.png';
  var routeAttrib = '&copy, Tiles © <a href="http://www.openstreetmap.fr">O'+
  'penStreetMap France</a>';
  var route = new L.TileLayer(routeUrl,
              {minZoom: 1, maxZoom: 20, attribution: routeAttrib});

  var baseLayers = {
        'OpenStreetMap': osm,
        'OpenCycleMap': osmCycle,
        'IGN France': ign,
        'OpenMapSurfer Roads': openmapsurfer,
        'Hike & bike': hikebike,
        'OSM Transport': transport,
        'ESRI Aerial': esriAerial,
        'ESRI Topo with relief': esriTopo,
        'Dark' : dark,
        'Toner' : toner,
        'Watercolor' : watercolor,
        'OpenStreetMap France': osmfr
  };
  // add custom layers
  $('#tileserverlist li').each(function(){
      var sname = $(this).attr('name');
      var surl = $(this).attr('title');
      baseLayers[sname] = new L.TileLayer(surl,
              {maxZoom: 18, attribution: 'custom tile server'});
  });
  gpxpod.baseLayers = baseLayers;
  var baseOverlays = {
      'OsmFr Route500': route,
      'OpenPisteMap Relief':
        L.tileLayer('http://tiles2.openpistemap.org/landshaded/{z}/{x}/{y}.png',
                    {
                    attribution: '&copy, Tiles © <a href="http://www.o'+
                    'penstreetmap.fr">OpenStreetMap France</a>',
                    minZoom: 1,
                    maxZoom: 15
                    }
        ),
      'OpenPisteMap pistes' : piste
  };

  //var layerlist = [osm,osmCycle,ign,openmapsurfer,hikebike,transport,
  //esriAerial,esriTopo,dark,toner,watercolor,osmfr];
  var layerlist = [];

  gpxpod.map = new L.Map('map', {
      zoomControl: true,
      layers: layerlist,
      //closePopupOnClick: false
  });

  L.control.scale({metric: true, imperial: true, position:'topleft'})
  .addTo(gpxpod.map);

  L.control.mousePosition().addTo(gpxpod.map);
  gpxpod.searchControl = L.Control.geocoder({position:'topleft'});
  gpxpod.searchControl.addTo(gpxpod.map);
  gpxpod.locateControl = L.control.locate({follow:true});
  gpxpod.locateControl.addTo(gpxpod.map);
  L.Control.measureControl().addTo(gpxpod.map);
  L.control.sidebar('sidebar').addTo(gpxpod.map);

  gpxpod.map.setView(new L.LatLng(27, 5), 3);

  if (! baseLayers.hasOwnProperty(default_layer)){
      default_layer = 'OpenStreetMap';
  }
  gpxpod.map.addLayer(baseLayers[default_layer]);

  gpxpod.activeLayers = L.control.activeLayers(baseLayers, baseOverlays);
  gpxpod.activeLayers.addTo(gpxpod.map);

  gpxpod.minimapControl = new L.Control.MiniMap(
          osmfr2,
          { toggleDisplay: true, position:'bottomleft' }
  ).addTo(gpxpod.map);
  gpxpod.minimapControl._toggleDisplayButtonClicked();

  //gpxpod.map.on('contextmenu',rightClick);
  //gpxpod.map.on('popupclose',function() {});
  //gpxpod.map.on('viewreset',updateTrackListFromBounds);
  //gpxpod.map.on('dragend',updateTrackListFromBounds);
  gpxpod.map.on('moveend',updateTrackListFromBounds);
  gpxpod.map.on('zoomend',updateTrackListFromBounds);
  gpxpod.map.on('baselayerchange',updateTrackListFromBounds);

}

//function rightClick(e) {
//    //new L.popup()
//    //    .setLatLng(e.latlng)
//    //    .setContent(preparepopup(e.latlng.lat,e.latlng.lng))
//    //    .openOn(gpxpod.map);
//}

function removeMarkers(){
    if (gpxpod.markerLayer !== null){
        gpxpod.map.removeLayer(gpxpod.markerLayer);
        delete gpxpod.markerLayer;
        gpxpod.markerLayer = null;
    }
}

// add markers respecting the filtering rules
function addMarkers(){
    var markerclu = L.markerClusterGroup({ chunkedLoading: true });
    var a, title, marker;
    for (var i = 0; i < gpxpod.markers.length; i++) {
        a = gpxpod.markers[i];
        if (filter(a)){
            title = a[NAME];
            marker = L.marker(L.latLng(a[LAT], a[LON]));
            marker.bindPopup(
                gpxpod.markersPopupTxt[title].popup,
                {
                    autoPan:true,
                    autoClose: true,
                    closeOnClick: true
                }
            );
            marker.bindTooltip(title);
            gpxpod.markersPopupTxt[title].marker = marker;
            markerclu.addLayer(marker);
        }
    }

    if ($('#displayclusters').is(':checked')){
        gpxpod.map.addLayer(markerclu);
    }
    //gpxpod.map.setView(new L.LatLng(47, 3), 2);

    gpxpod.markerLayer = markerclu;

    //markers.on('clusterclick', function (a) {
    //   var bounds = a.layer.getConvexHull();
    //   updateTrackListFromBounds(bounds);
    //});
}

// return true if the marker respects all filters
function filter(m){
    var mdate = new Date(m[DATE_END].split(' ')[0]);
    var mdist = m[TOTAL_DISTANCE];
    var mceg = m[POSITIVE_ELEVATION_GAIN];
    var datemin = $('#datemin').val();
    var datemax = $('#datemax').val();
    var distmin = $('#distmin').val();
    var distmax = $('#distmax').val();
    var cegmin = $('#cegmin').val();
    var cegmax = $('#cegmax').val();

    if (datemin !== ''){
        var ddatemin = new Date(datemin);
        if (mdate < ddatemin){
            return false;
        }
    }
    if (datemax !== ''){
        var ddatemax = new Date(datemax);
        if (ddatemax < mdate){
            return false;
        }
    }
    if (distmin !== ''){
        if (mdist < distmin){
            return false;
        }
    }
    if (distmax !== ''){
        if (distmax < mdist){
            return false;
        }
    }
    if (cegmin !== ''){
        if (mceg < cegmin){
            return false;
        }
    }
    if (cegmax !== ''){
        if (cegmax < mceg){
            return false;
        }
    }

    return true;
}

function clearFiltersValues(){
    $('#datemin').val('');
    $('#datemax').val('');
    $('#distmin').val('');
    $('#distmax').val('');
    $('#cegmin').val('');
    $('#cegmax').val('');
}

function updateTrackListFromBounds(e){

    var m;
    var table_rows = '';
    var hassrtm = ($('#processtypeselect option').length > 2);
    var mapBounds = gpxpod.map.getBounds();
    var chosentz = $('#tzselect').val();
    var activeLayerName = gpxpod.activeLayers.getActiveBaseLayer().name;
    var url = OC.generateUrl('/apps/files/ajax/download.php');
    // state of "update table" option checkbox
    var updOption = $('#updtracklistcheck').is(':checked');
    var tablecriteria = $('#tablecriteriasel').val();
    var subfo = gpxpod.subfolder;
    if (subfo === '/'){
        subfo = '';
    }

    // if this is a public link, the url is the public share
    if (pageIsPublicFolder()){
        var url = OC.generateUrl('/s/'+gpxpod.token+
                '/download?path=&files=');
    }
    else if (pageIsPublicFile()){
        var url = OC.generateUrl('/s/'+gpxpod.token);
    }

    for (var i = 0; i < gpxpod.markers.length; i++) {
        m = gpxpod.markers[i];
        if (filter(m)){
            //if ((!updOption) || mapBounds.contains(new L.LatLng(m[LAT], m[LON]))){
            if ((!updOption) ||
                    (tablecriteria == 'bounds' && mapBounds.intersects(
                        new L.LatLngBounds(
                            new L.LatLng(m[SOUTH], m[WEST]),
                            new L.LatLng(m[NORTH], m[EAST])
                            )
                        )
                    ) ||
                    (tablecriteria == 'start' &&
                     mapBounds.contains(new L.LatLng(m[LAT], m[LON]))) ||
                    (tablecriteria == 'cross' &&
                     trackCrossesMapBounds(m[SHORTPOINTLIST], mapBounds))
               ){
                if (gpxpod.gpxlayers.hasOwnProperty(m[NAME])){
                    table_rows = table_rows+'<tr><td style="background:'+
                    gpxpod.gpxlayers[m[NAME]].color+'"><input type="checkbox"';
                    table_rows = table_rows+' checked="checked" ';
                }
                else{
                    table_rows = table_rows+'<tr><td><input type="checkbox"';
                }
                table_rows = table_rows+' class="drawtrack" id="'+
                             escapeHTML(m[NAME])+'"></td>\n';
                table_rows = table_rows+
                             '<td class="trackname"><div class="trackcol">';

                var dl_url = '';
                if (pageIsPublicFolder()){
                    dl_url = '"'+url+escapeHTML(m[NAME])+'" target="_blank"';
                }
                else if (pageIsPublicFile()){
                    dl_url = '"'+url+'" target="_blank"';
                }
                else{
                    dl_url = '"'+url+'?dir='+gpxpod.subfolder+'&files='+escapeHTML(m[NAME])+'"';
                }
                table_rows = table_rows + '<a href='+dl_url+
                    ' title="'+t('gpxpod','download')+'" class="tracklink">'+
                    '<i class="fa fa-cloud-download" aria-hidden="true"></i>'+
                    escapeHTML(m[NAME])+'</a>\n';

                table_rows = table_rows + '<div>';

                if (! pageIsPublicFileOrFolder()){
                    if (hassrtm){
                        table_rows = table_rows + '<a href="#" track="'+
                            escapeHTML(m[NAME])+'" class="csrtms" title="'+
                            t('gpxpod','Correct elevations with smoothing for this track')+'">'+
                            '<i class="fa fa-line-chart" aria-hidden="true"></i>'+
                            '</a>';
                        table_rows = table_rows + '<a href="#" track="'+
                            escapeHTML(m[NAME])+'" class="csrtm" title="'+
                            t('gpxpod','Correct elevations for this track')+'">'+
                            '<i class="fa fa-line-chart" aria-hidden="true"></i>'+
                            '</a>';
                    }
                    if (gpxpod.gpxedit_compliant){
                        var edurl = gpxpod.gpxedit_url + 'file='+encodeURI(subfo+'/'+m[NAME]);
                        table_rows = table_rows + '<a href="'+edurl+'" '+
                            'target="_blank" class="editlink" title="'+
                            t('gpxpod','Edit this file in GpxEdit')+'">'+
                            '<i class="fa fa-pencil" aria-hidden="true"></i>'+
                            '</a>';
                    }
                    table_rows = table_rows +' <a class="permalink publink" '+
                    'type="track" name="'+escapeHTML(m[NAME])+'"'+
                    'title="'+
                    escapeHTML(t('gpxpod','This public link will work only if "{title}'+
                    '" or one of its parent folder is '+
                    'shared in "files" app by public link without password', {title:escapeHTML(m[NAME])}))+
                    '" target="_blank" href="publink?filepath='+encodeURI(subfo+
                    '/'+m[NAME])+'&user='+encodeURI(gpxpod.username)+'">'+
                    '<i class="fa fa-share-alt" aria-hidden="true"></i></a>';
                }

                table_rows = table_rows + '</div>';

                table_rows = table_rows +'</div></td>\n';
                var datestr = 'no date';
                try{
                    if (m[DATE_END] !== '' && m[DATE_END] !== 'None'){
                        var mom = moment(m[DATE_END].replace(' ','T')+'Z');
                        mom.tz(chosentz);
                        datestr = mom.format('YYYY-MM-DD');
                    }
                }
                catch(err){
                }
                table_rows = table_rows + '<td>'+
                             escapeHTML(datestr)+'</td>\n';
                table_rows = table_rows +
                '<td>'+(m[TOTAL_DISTANCE]/1000).toFixed(2)+'</td>\n';

                table_rows = table_rows +
                '<td><div class="durationcol">'+
                escapeHTML(m[TOTAL_DURATION])+'</div></td>\n';

                table_rows = table_rows +
                '<td>'+escapeHTML(m[POSITIVE_ELEVATION_GAIN])+'</td>\n';
                table_rows = table_rows + '</tr>\n';
            }
        }
    }

    if (table_rows === ''){
        var table = '';
        $('#gpxlist').html(table);
        //$('#ticv').hide();
        $('#ticv').text(t('gpxpod','No track visible'));
    }
    else{
        //$('#ticv').show();
        if ($('#updtracklistcheck').is(':checked')){
            $('#ticv').text(t('gpxpod','Tracks from current view'));
        }
        else{
            $('#ticv').text(t('gpxpod','All tracks'));
        }
        var table = '<table id="gpxtable" class="tablesorter">\n<thead>';
        table = table + '<tr>';
        table = table + '<th title="'+t('gpxpod','Draw')+'">'+
            '<i class="bigfa fa fa-pencil-square-o" aria-hidden="true"></i></th>\n';
        table = table + '<th>'+t('gpxpod','Track')+'</th>\n';
        table = table + '<th>'+t('gpxpod','Date')+
            '<br/><i class="fa fa-calendar" aria-hidden="true"></i></th>\n';
        table = table + '<th>'+t('gpxpod','Dist<br/>ance<br/>(km)')+
            '<br/><i class="fa fa-arrows-h" aria-hidden="true"></i></th>\n';
        table = table + '<th>'+t('gpxpod','Duration')+
            '<br/><i class="fa fa-clock-o" aria-hidden="true"></i></th>\n';
        table = table + '<th>'+t('gpxpod','Cumulative<br/>elevation<br/>gain (m)')+
            '<br/><i class="fa fa-line-chart" aria-hidden="true"></i></th>\n';
        table = table + '</tr></thead><tbody>\n';
        table = table + table_rows;
        table = table + '</tbody></table>';
        $('#gpxlist').html(table);
        $('#gpxtable').tablesorter({
            widthFixed: false,
            sortList: [gpxpod.tablesortCol],
            dateFormat: 'yyyy-mm-dd',
            headers: {
                2: {sorter: 'shortDate', string: 'min'},
                3: {sorter: 'digit', string: 'min'},
                4: {sorter: 'time'},
                5: {sorter: 'digit', string: 'min'},
            }
        });
    }
}

/*
 * display markers if the checkbox is checked
 */
function redraw()
{
    // remove markers if they are present
    removeMarkers();
    addMarkers();
    return;

}

function layerBringToFront(l){
    l.bringToFront();
}

function addColoredTrackDraw(gpx, tid, withElevation){
    deleteOnHover();

    var color = 'red';
    var lineBorder = $('#linebordercheck').is(':checked');
    var colorCriteria = $('#colorcriteria').val();
    var yUnit = 'm';
    if (colorCriteria === 'speed'){
        yUnit = 'km/h';
    }

    var gpxx = $(gpx);

    if (gpxpod.gpxlayers.hasOwnProperty(tid)){
        console.log('remove '+tid);
        removeTrackDraw(tid);
    }

    // count the number of lines and point
    var nbPoints = gpxx.find('>wpt').length;
    var nbLines = gpxx.find('>trk').length + gpxx.find('>rte').length;

    if (withElevation){
        removeElevation();
        if (nbLines>0){
            var el = L.control.elevation({
                position:'bottomright',
                height:100,
                width:700,
                margins: {
                    top: 10,
                    right: 80,
                    bottom: 30,
                    left: 80
                },
                yUnit: yUnit,
                theme: 'steelblue-theme'
            });
            el.addTo(gpxpod.map);
            gpxpod.elevationLayer = el;
            gpxpod.elevationTrack = tid;
        }
    }

    if (! gpxpod.gpxlayers.hasOwnProperty(tid)){
        var whatToDraw = $('#trackwaypointdisplayselect').val();
        var weight = parseInt($('#lineweight').val());
        var waypointStyle = getWaypointStyle();
        var tooltipStyle = getTooltipStyle();
        var symbolOverwrite = getSymbolOverwrite();

        var gpxlayer = {color: 'linear-gradient(to right, lightgreen, yellow, red);'};
        gpxlayer['layer'] = L.featureGroup();
        gpxlayer['layerOutlines'] = null;

        var fileDesc = gpxx.find('>metadata>desc').text();

        if (whatToDraw !== 't'){
            gpxx.find('wpt').each(function(){
                var lat = $(this).attr('lat');
                var lon = $(this).attr('lon');
                var name = $(this).find('name').text();
                var cmt = $(this).find('cmt').text();
                var desc = $(this).find('desc').text();
                var sym = $(this).find('sym').text();
                var ele = $(this).find('ele').text();
                var time = $(this).find('time').text();

                var mm = L.marker(
                    [lat, lon],
                    {
                        icon: symbolIcons[waypointStyle]
                    }
                );
                if (tooltipStyle === 'p'){
                    mm.bindTooltip(brify(name, 20), {permanent: true, className: 'tooltip'+color});
                }
                else{
                    mm.bindTooltip(brify(name, 20), {className: 'tooltip'+color});
                }

                var popupText = '<h3 style="text-align:center;">'+name + '</h3><hr/>';
                t('gpxpod','Track')+ ' : '+tid+'<br/>';
                if (ele !== ''){
                    popupText = popupText+t('gpxpod','Elevation')+ ' : '+
                        ele + 'm<br/>';
                }
                popupText = popupText+t('gpxpod','Latitude')+' : '+ lat + '<br/>'+
                    t('gpxpod','Longitude')+' : '+ lon+'<br/>';
                if (cmt !== ''){
                    popupText = popupText+
                        t('gpxpod','Comment')+' : '+ cmt+'<br/>';
                }
                if (desc !== ''){
                    popupText = popupText+
                        t('gpxpod','Description')+' : '+desc+'<br/>';
                }
                if (sym !== ''){
                    popupText = popupText+
                        t('gpxpod','Symbol name')+' : '+ sym;
                }
                if (symbolOverwrite && sym){
                    if (symbolIcons.hasOwnProperty(sym)){
                        mm.setIcon(symbolIcons[sym]);
                    }
                    else{
                        mm.setIcon(L.divIcon({
                            className: 'unknown',
                            iconAnchor: [12, 12]
                        }));
                    }
                }
                mm.bindPopup(popupText);
                gpxlayer['layer'].addLayer(mm);
            });
        }

        if (whatToDraw !== 'w'){
            gpxx.find('trk').each(function(){
                var name = $(this).find('>name').text();
                var cmt = $(this).find('>cmt').text();
                var desc = $(this).find('>desc').text();
                $(this).find('trkseg').each(function(){
                    if (colorCriteria === 'elevation'){
                        var latlngs = [];
                        var times = [];
                        var prevEle = null;
                        var minVal = null;
                        var maxVal = null;
                        $(this).find('trkpt').each(function(){
                            var lat = $(this).attr('lat');
                            var lon = $(this).attr('lon');
                            var ele = $(this).find('ele').text();
                            var time = $(this).find('time').text();
                            times.push(time);
                            if (ele !== ''){
                                ele = parseFloat(ele);
                                if (minVal === null || ele < minVal){
                                    minVal = ele;
                                }
                                if (maxVal === null || ele > maxVal){
                                    maxVal = ele;
                                }
                                latlngs.push([lat,lon,ele]);
                            }
                            else{
                                latlngs.push([lat,lon,0]);
                            }
                        });
                    }
                    else if (colorCriteria === 'speed'){
                        var latlngs = [];
                        var times = [];
                        var prevLatLng = null;
                        var prevDateTime = null;
                        var minVal = null;
                        var maxVal = null;
                        var latlng;
                        var date;
                        var dateTime;
                        $(this).find('trkpt').each(function(){
                            var lat = $(this).attr('lat');
                            var lon = $(this).attr('lon');
                            latlng = L.latLng(lat, lon);
                            var ele = $(this).find('ele').text();
                            var time = $(this).find('time').text();
                            times.push(time);
                            if (time !== ''){
                                date = new Date(time);
                                dateTime = date.getTime();
                            }

                            if (time !== '' && prevDateTime !== null){
                                var dist = latlng.distanceTo(prevLatLng);
                                var speed = dist / ((dateTime - prevDateTime) / 1000) * 3.6;
                                if (minVal === null || speed < minVal){
                                    minVal = speed;
                                }
                                if (maxVal === null || speed > maxVal){
                                    maxVal = speed;
                                }
                                latlngs.push([lat,lon,speed]);
                            }
                            else{
                                latlngs.push([lat,lon,0]);
                            }

                            // keep some previous values
                            prevLatLng = latlng;
                            if (time !== ''){
                                prevDateTime = dateTime;
                            }
                            else{
                                prevDateTime = null;
                            }
                        });
                    }

                    var outlineWidth = 0.3*weight;
                    if (!lineBorder){
                        outlineWidth = 0;
                    }
                    var l = L.hotline(latlngs, {
                        weight: weight,
                        outlineWidth: outlineWidth,
                        min: minVal,
                        max: maxVal
                    });
                    var popupText = gpxpod.markersPopupTxt[tid].popup;
                    if (cmt !== ''){
                        popupText = popupText + '<p class="combutton" combutforfeat="'+tid+name+
                            '" style="margin:0; cursor:pointer;">'+t('gpxpod','Comment')+' <i class="fa fa-expand"></i></p>'+
                            '<p class="comtext" style="display:none; margin:0; cursor:pointer;" comforfeat="'+tid+name+'">'+
                            cmt + '</p>';
                    }
                    if (desc !== ''){
                        popupText = popupText + '<p class="descbutton" descbutforfeat="'+tid+name+
                            '" style="margin:0; cursor:pointer;">Description <i class="fa fa-expand"></i></p>'+
                            '<p class="desctext" style="display:none; margin:0; cursor:pointer;" descforfeat="'+tid+name+'">'+
                            desc + '</p>';
                    }
                    popupText = popupText.replace('<li>'+name+'</li>', '<li><b style="color:blue;">'+name+'</b></li>');
                    l.bindPopup(
                            popupText,
                            {
                                autoPan:true,
                                autoClose: true,
                                closeOnClick: true
                            }
                    );
                    var tooltipText = tid;
                    if (tid !== name){
                        tooltipText = tooltipText+'<br/>'+name;
                    }
                    if (tooltipStyle === 'p'){
                        l.bindTooltip(tooltipText, {permanent:true});
                    }
                    else{
                        l.bindTooltip(tooltipText, {sticky:true});
                    }
                    if (withElevation){
                        var data = l.toGeoJSON();
                        if (times.length === data.geometry.coordinates.length){
                            for (var i=0; i<data.geometry.coordinates.length; i++){
                                data.geometry.coordinates[i].push(times[i]);
                            }
                        }
                        el.addData(data, l)
                    }
                    l.on('mouseover', function(){
                        hoverStyle.weight = parseInt(2*weight);
                        defaultStyle.weight = weight;
                        l.setStyle(hoverStyle);
                        defaultStyle.color = color;
                        gpxpod.gpxlayers[tid]['layer'].bringToFront();
                    });
                    l.on('mouseout', function(){
                        l.setStyle(defaultStyle);
                    });

                    gpxlayer['layer'].addLayer(l);
                });
            });
            gpxx.find('rte').each(function(){
                var name = $(this).find('>name').text();
                var cmt = $(this).find('>cmt').text();
                var desc = $(this).find('>desc').text();
                if (colorCriteria === 'elevation'){
                    var latlngs = [];
                    var times = [];
                    var prevEle = null;
                    var minVal = null;
                    var maxVal = null;
                    $(this).find('rtept').each(function(){
                        var lat = $(this).attr('lat');
                        var lon = $(this).attr('lon');
                        var ele = $(this).find('ele').text();
                        var time = $(this).find('time').text();
                        times.push(time);
                        if (ele !== ''){
                            ele = parseFloat(ele);
                            if (minVal === null || ele < minVal){
                                minVal = ele;
                            }
                            if (maxVal === null || ele > maxVal){
                                maxVal = ele;
                            }
                            latlngs.push([lat,lon,ele]);
                        }
                        else{
                            latlngs.push([lat,lon,0]);
                        }
                    });
                }
                else if (colorCriteria === 'speed'){
                    var latlngs = [];
                    var times = [];
                    var prevLatLng = null;
                    var prevDateTime = null;
                    var minVal = null;
                    var maxVal = null;
                    var latlng;
                    var date;
                    var dateTime;
                    $(this).find('rtept').each(function(){
                        var lat = $(this).attr('lat');
                        var lon = $(this).attr('lon');
                        latlng = L.latLng(lat, lon);
                        var ele = $(this).find('ele').text();
                        var time = $(this).find('time').text();
                        times.push(time);
                        if (time !== ''){
                            date = new Date(time);
                            dateTime = date.getTime();
                        }

                        if (time !== '' && prevDateTime !== null){
                            var dist = latlng.distanceTo(prevLatLng);
                            var speed = dist / ((dateTime - prevDateTime) / 1000) * 3.6;
                            if (minVal === null || speed < minVal){
                                minVal = speed;
                            }
                            if (maxVal === null || speed > maxVal){
                                maxVal = speed;
                            }
                            latlngs.push([lat,lon,speed]);
                        }
                        else{
                            latlngs.push([lat,lon,0]);
                        }

                        // keep some previous values
                        prevLatLng = latlng;
                        if (time !== ''){
                            prevDateTime = dateTime;
                        }
                        else{
                            prevDateTime = null;
                        }
                    });
                }

                var outlineWidth = 0.3*weight;
                if (!lineBorder){
                    outlineWidth = 0;
                }
                var l = L.hotline(latlngs, {
                    weight: weight,
                    outlineWidth: outlineWidth,
                    min: minVal,
                    max: maxVal
                });
                var popupText = gpxpod.markersPopupTxt[tid].popup;
                if (cmt !== ''){
                    popupText = popupText + '<p class="combutton" combutforfeat="'+tid+name+
                        '" style="margin:0; cursor:pointer;">'+t('gpxpod','Comment')+' <i class="fa fa-expand"></i></p>'+
                        '<p class="comtext" style="display:none; margin:0; cursor:pointer;" comforfeat="'+tid+name+'">'+
                        cmt + '</p>';
                }
                if (desc !== ''){
                    popupText = popupText + '<p class="descbutton" descbutforfeat="'+tid+name+
                        '" style="margin:0; cursor:pointer;">Description <i class="fa fa-expand"></i></p>'+
                        '<p class="desctext" style="display:none; margin:0; cursor:pointer;" descforfeat="'+tid+name+'">'+
                        desc + '</p>';
                }
                popupText = popupText.replace('<li>'+name+'</li>', '<li><b style="color:blue;">'+name+'</b></li>');
                l.bindPopup(
                        popupText,
                        {
                            autoPan:true,
                            autoClose: true,
                            closeOnClick: true
                        }
                );
                var tooltipText = tid;
                if (tid !== name){
                    tooltipText = tooltipText+'<br/>'+name;
                }
                if (tooltipStyle === 'p'){
                    l.bindTooltip(tooltipText, {permanent:true});
                }
                else{
                    l.bindTooltip(tooltipText, {sticky:true});
                }
                if (withElevation){
                    var data = l.toGeoJSON();
                    if (times.length === data.geometry.coordinates.length){
                        for (var i=0; i<data.geometry.coordinates.length; i++){
                            data.geometry.coordinates[i].push(times[i]);
                        }
                    }
                    el.addData(data, l)
                }
                l.on('mouseover', function(){
                    hoverStyle.weight = parseInt(2*weight);
                    defaultStyle.weight = weight;
                    l.setStyle(hoverStyle);
                    defaultStyle.color = color;
                    gpxpod.gpxlayers[tid]['layer'].bringToFront();
                });
                l.on('mouseout', function(){
                    l.setStyle(defaultStyle);
                });

                gpxlayer['layer'].addLayer(l);
            });
        }

        gpxlayer.layer.addTo(gpxpod.map);
        gpxpod.gpxlayers[tid] = gpxlayer;

        // zoom is made only if a normal track is drawn
        // if it's just for elevation, do not zoom
        if ($('#autozoomcheck').is(':checked')){
            gpxpod.map.fitBounds(gpxlayer.layer.getBounds(),
                    {animate:true, paddingTopLeft: [parseInt($('#sidebar').css('width')),0]}
            );
        }


        updateTrackListFromBounds();
        gpxpod.map.closePopup();
        if ($('#openpopupcheck').is(':checked') && nbLines > 0){
            // open popup on the marker position,
            // works better than opening marker popup
            // because the clusters avoid popup opening when marker is
            // not visible because it's grouped
            var pop = L.popup({
                autoPan:true,
                autoClose: true,
                closeOnClick: true
            });
            pop.setContent(gpxpod.markersPopupTxt[tid].popup);
            pop.setLatLng(gpxpod.markersPopupTxt[tid].marker.getLatLng());
            pop.openOn(gpxpod.map);
        }
    }
}

function addTrackDraw(gpx, tid, withElevation){
    deleteOnHover();

    var lineBorder = $('#linebordercheck').is(':checked');
    // choose color
    var color;
    color=colors[++lastColorUsed % colors.length];

    var gpxx = $(gpx);

    // count the number of lines and point
    var nbPoints = gpxx.find('>wpt').length;
    var nbLines = gpxx.find('>trk').length + gpxx.find('>rte').length;

    if (withElevation){
        removeElevation();
        if (nbLines>0){
            var el = L.control.elevation({
                position:'bottomright',
                height:100,
                width:700,
                margins: {
                    top: 10,
                    right: 80,
                    bottom: 30,
                    left: 60
                },
                theme: 'steelblue-theme'
            });
            el.addTo(gpxpod.map);
            gpxpod.elevationLayer = el;
            gpxpod.elevationTrack = tid;
        }
    }

    if ( (! gpxpod.gpxlayers.hasOwnProperty(tid))){
        var whatToDraw = $('#trackwaypointdisplayselect').val();
        var weight = parseInt($('#lineweight').val());
        var waypointStyle = getWaypointStyle();
        var tooltipStyle = getTooltipStyle();
        var symbolOverwrite = getSymbolOverwrite();

        var gpxlayer = {color: color};
        gpxlayer['layerOutlines'] = L.layerGroup();
        gpxlayer['layer'] = L.featureGroup();

        var fileDesc = gpxx.find('>metadata>desc').text();

        if (whatToDraw !== 't'){
            gpxx.find('wpt').each(function(){
                var lat = $(this).attr('lat');
                var lon = $(this).attr('lon');
                var name = $(this).find('name').text();
                var cmt = $(this).find('cmt').text();
                var desc = $(this).find('desc').text();
                var sym = $(this).find('sym').text();
                var ele = $(this).find('ele').text();
                var time = $(this).find('time').text();

                var mm = L.marker(
                    [lat, lon],
                    {
                        icon: symbolIcons[waypointStyle]
                    }
                );
                if (tooltipStyle === 'p'){
                    mm.bindTooltip(brify(name, 20), {permanent: true, className: 'tooltip'+color});
                }
                else{
                    mm.bindTooltip(brify(name, 20), {className: 'tooltip'+color});
                }

                var popupText = '<h3 style="text-align:center;">'+name + '</h3><hr/>';
                t('gpxpod','Track')+ ' : '+tid+'<br/>';
                if (ele !== ''){
                    popupText = popupText+t('gpxpod','Elevation')+ ' : '+
                        ele + 'm<br/>';
                }
                popupText = popupText+t('gpxpod','Latitude')+' : '+ lat + '<br/>'+
                    t('gpxpod','Longitude')+' : '+ lon+'<br/>';
                if (cmt !== ''){
                    popupText = popupText+
                        t('gpxpod','Comment')+' : '+ cmt+'<br/>';
                }
                if (desc !== ''){
                    popupText = popupText+
                        t('gpxpod','Description')+' : '+ desc+'<br/>';
                }
                if (sym !== ''){
                    popupText = popupText+
                        t('gpxpod','Symbol name')+' : '+ sym;
                }
                if (symbolOverwrite && sym){
                    if (symbolIcons.hasOwnProperty(sym)){
                        mm.setIcon(symbolIcons[sym]);
                    }
                    else{
                        mm.setIcon(L.divIcon({
                            className: 'unknown',
                            iconAnchor: [12, 12]
                        }));
                    }
                }
                mm.bindPopup(popupText);
                gpxlayer['layer'].addLayer(mm);
            });
        }

        if (whatToDraw !== 'w'){
            gpxx.find('trk').each(function(){
                var name = $(this).find('>name').text();
                var cmt = $(this).find('>cmt').text();
                var desc = $(this).find('>desc').text();
                $(this).find('trkseg').each(function(){
                    var latlngs = [];
                    var times = [];
                    $(this).find('trkpt').each(function(){
                        var lat = $(this).attr('lat');
                        var lon = $(this).attr('lon');
                        var ele = $(this).find('ele').text();
                        var time = $(this).find('time').text();
                        times.push(time);
                        if (ele !== ''){
                            latlngs.push([lat,lon,ele]);
                        }
                        else{
                            latlngs.push([lat,lon]);
                        }
                    });
                    var l = L.polyline(latlngs,{
                        weight: weight,
                        opacity : 1,
                        color: color,
                    });
                    var popupText = gpxpod.markersPopupTxt[tid].popup;
                    if (cmt !== ''){
                        popupText = popupText + '<p class="combutton" combutforfeat="'+tid+name+
                            '" style="margin:0; cursor:pointer;">'+t('gpxpod','Comment')+' <i class="fa fa-expand"></i></p>'+
                            '<p class="comtext" style="display:none; margin:0; cursor:pointer;" comforfeat="'+tid+name+'">'+
                            cmt + '</p>';
                    }
                    if (desc !== ''){
                        popupText = popupText + '<p class="descbutton" descbutforfeat="'+tid+name+
                            '" style="margin:0; cursor:pointer;">Description <i class="fa fa-expand"></i></p>'+
                            '<p class="desctext" style="display:none; margin:0; cursor:pointer;" descforfeat="'+tid+name+'">'+
                            desc + '</p>';
                    }
                    popupText = popupText.replace('<li>'+name+'</li>', '<li><b style="color:blue;">'+name+'</b></li>');
                    l.bindPopup(
                            popupText,
                            {
                                autoPan:true,
                                autoClose: true,
                                closeOnClick: true
                            }
                    );
                    var tooltipText = tid;
                    if (tid !== name){
                        tooltipText = tooltipText+'<br/>'+name;
                    }
                    if (tooltipStyle === 'p'){
                        l.bindTooltip(tooltipText, {permanent:true, className: 'tooltip'+color});
                    }
                    else{
                        l.bindTooltip(tooltipText, {sticky:true, className: 'tooltip'+color});
                    }
                    if (withElevation){
                        var data = l.toGeoJSON();
                        if (times.length === data.geometry.coordinates.length){
                            for (var i=0; i<data.geometry.coordinates.length; i++){
                                data.geometry.coordinates[i].push(times[i]);
                            }
                        }
                        el.addData(data, l)
                    }
                    // border layout
                    var bl;
                    if (lineBorder){
                        bl = L.polyline(latlngs,
                            {opacity:1, weight: parseInt(weight*1.6), color:'black'});
                        gpxlayer['layerOutlines'].addLayer(bl);
                        bl.on('mouseover', function(){
                            hoverStyle.weight = parseInt(2*weight);
                            defaultStyle.weight = weight;
                            l.setStyle(hoverStyle);
                            defaultStyle.color = color;
                            gpxpod.gpxlayers[tid]['layerOutlines'].eachLayer(layerBringToFront);
                            //layer.bringToFront();
                            gpxpod.gpxlayers[tid]['layer'].bringToFront();
                        });
                        bl.on('mouseout', function(){
                            l.setStyle(defaultStyle);
                        });
                        if (tooltipStyle !== 'p'){
                            bl.bindTooltip(tooltipText, {sticky:true, className: 'tooltip'+color});
                        }
                    }
                    l.on('mouseover', function(){
                        hoverStyle.weight = parseInt(2*weight);
                        defaultStyle.weight = weight;
                        l.setStyle(hoverStyle);
                        defaultStyle.color = color;
                        if (lineBorder){
                            gpxpod.gpxlayers[tid]['layerOutlines'].eachLayer(layerBringToFront);
                        }
                        //layer.bringToFront();
                        gpxpod.gpxlayers[tid]['layer'].bringToFront();
                    });
                    l.on('mouseout', function(){
                        l.setStyle(defaultStyle);
                    });

                    gpxlayer['layer'].addLayer(l);
                });
            });

            // ROUTES
            gpxx.find('rte').each(function(){
                var name = $(this).find('>name').text();
                var cmt = $(this).find('>cmt').text();
                var desc = $(this).find('>desc').text();
                var latlngs = [];
                var times = [];
                $(this).find('rtept').each(function(){
                    var lat = $(this).attr('lat');
                    var lon = $(this).attr('lon');
                    var ele = $(this).find('ele').text();
                    var time = $(this).find('time').text();
                    times.push(time);
                    if (ele !== ''){
                        latlngs.push([lat,lon,ele]);
                    }
                    else{
                        latlngs.push([lat,lon]);
                    }
                });
                var l = L.polyline(latlngs,{
                    weight: weight,
                    opacity : 1,
                    color: color,
                });
                var popupText = gpxpod.markersPopupTxt[tid].popup;
                if (cmt !== ''){
                    popupText = popupText + '<p class="combutton" combutforfeat="'+tid+name+
                        '" style="margin:0; cursor:pointer;">'+t('gpxpod','Comment')+' <i class="fa fa-expand"></i></p>'+
                        '<p class="comtext" style="display:none; margin:0; cursor:pointer;" comforfeat="'+tid+name+'">'+
                        cmt + '</p>';
                }
                if (desc !== ''){
                    popupText = popupText + '<p class="descbutton" descbutforfeat="'+tid+name+
                        '" style="margin:0; cursor:pointer;">Description <i class="fa fa-expand"></i></p>'+
                        '<p class="desctext" style="display:none; margin:0; cursor:pointer;" descforfeat="'+tid+name+'">'+
                        desc + '</p>';
                }
                popupText = popupText.replace('<li>'+name+'</li>', '<li><b style="color:blue;">'+name+'</b></li>');
                l.bindPopup(
                        popupText,
                        {
                            autoPan:true,
                            autoClose: true,
                            closeOnClick: true
                        }
                );
                var tooltipText = tid;
                if (tid !== name){
                    tooltipText = tooltipText+'<br/>'+name;
                }
                if (tooltipStyle === 'p'){
                    l.bindTooltip(tooltipText, {permanent:true, className: 'tooltip'+color});
                }
                else{
                    l.bindTooltip(tooltipText, {sticky:true, className: 'tooltip'+color});
                }
                if (withElevation){
                    var data = l.toGeoJSON();
                    if (times.length === data.geometry.coordinates.length){
                        for (var i=0; i<data.geometry.coordinates.length; i++){
                            data.geometry.coordinates[i].push(times[i]);
                        }
                    }
                    el.addData(data, l)
                }
                // border layout
                var bl;
                if (lineBorder){
                    bl = L.polyline(latlngs,
                        {opacity:1, weight: parseInt(weight*1.6), color:'black'});
                    gpxlayer['layerOutlines'].addLayer(bl);
                    bl.on('mouseover', function(){
                        hoverStyle.weight = parseInt(2*weight);
                        defaultStyle.weight = weight;
                        l.setStyle(hoverStyle);
                        defaultStyle.color = color;
                        gpxpod.gpxlayers[tid]['layerOutlines'].eachLayer(layerBringToFront);
                        //layer.bringToFront();
                        gpxpod.gpxlayers[tid]['layer'].bringToFront();
                    });
                    bl.on('mouseout', function(){
                        l.setStyle(defaultStyle);
                    });
                    if (tooltipStyle !== 'p'){
                        bl.bindTooltip(tooltipText, {sticky:true, className: 'tooltip'+color});
                    }
                }
                l.on('mouseover', function(){
                    hoverStyle.weight = parseInt(2*weight);
                    defaultStyle.weight = weight;
                    l.setStyle(hoverStyle);
                    defaultStyle.color = color;
                    if (lineBorder){
                        gpxpod.gpxlayers[tid]['layerOutlines'].eachLayer(layerBringToFront);
                    }
                    //layer.bringToFront();
                    gpxpod.gpxlayers[tid]['layer'].bringToFront();
                });
                l.on('mouseout', function(){
                    l.setStyle(defaultStyle);
                });

                gpxlayer['layer'].addLayer(l);
            });
        }

        gpxlayer.layerOutlines.addTo(gpxpod.map);
        gpxlayer.layer.addTo(gpxpod.map);
        gpxpod.gpxlayers[tid] = gpxlayer;

        // zoom is made only if a normal track is drawn
        // if it's just for elevation, do not zoom
        if ($('#autozoomcheck').is(':checked')){
            gpxpod.map.fitBounds(gpxlayer.layer.getBounds(),
                    {animate:true, paddingTopLeft: [parseInt($('#sidebar').css('width')),0]}
            );
        }

        updateTrackListFromBounds();
        gpxpod.map.closePopup();
        if ($('#openpopupcheck').is(':checked') && nbLines > 0){
            // open popup on the marker position,
            // works better than opening marker popup
            // because the clusters avoid popup opening when marker is
            // not visible because it's grouped
            var pop = L.popup({
                autoPan:true,
                autoClose: true,
                closeOnClick: true
            });
            pop.setContent(gpxpod.markersPopupTxt[tid].popup);
            pop.setLatLng(gpxpod.markersPopupTxt[tid].marker.getLatLng());
            pop.openOn(gpxpod.map);
        }
    }
}

function removeTrackDraw(tid){
    if (gpxpod.gpxlayers.hasOwnProperty(tid) &&
            (gpxpod.gpxlayers[tid].hasOwnProperty('layer')) &&
            gpxpod.map.hasLayer(gpxpod.gpxlayers[tid].layer)){
        gpxpod.map.removeLayer(gpxpod.gpxlayers[tid].layer);
        if (gpxpod.gpxlayers[tid].layerOutlines !== null){
            gpxpod.map.removeLayer(gpxpod.gpxlayers[tid].layerOutlines);
        }
        delete gpxpod.gpxlayers[tid].layer;
        delete gpxpod.gpxlayers[tid].layerOutlines;
        delete gpxpod.gpxlayers[tid].color;
        delete gpxpod.gpxlayers[tid];
        updateTrackListFromBounds();
        if (gpxpod.elevationTrack === tid){
            removeElevation();
        }
    }
}

function genPopupTxt(){
    gpxpod.markersPopupTxt = {};
    var chosentz = $('#tzselect').val();
    var url = OC.generateUrl('/apps/files/ajax/download.php');
    var subfo = gpxpod.subfolder;
    if (subfo === '/'){
        subfo = '';
    }
    // if this is a public link, the url is the public share
    if (pageIsPublicFileOrFolder()){
        var url = OC.generateUrl('/s/'+gpxpod.token);
    }
    for (var i = 0; i < gpxpod.markers.length; i++) {
        var a = gpxpod.markers[i];
        var title = escapeHTML(a[NAME]);

        if (pageIsPublicFolder()){
            dl_url = '"'+url+'/download?path=&files='+title+'" target="_blank"';
        }
        else if (pageIsPublicFile()){
            dl_url = '"'+url+'" target="_blank"';
        }
        else{
            var dl_url = '"'+url+'?dir='+gpxpod.subfolder+'&files='+title+'"';
        }

        var popupTxt = '<h3 style="text-align:center;">'+
            t('gpxpod','File')+' : <a href='+
            dl_url+' title="'+t('gpxpod','download')+'" class="getGpx" >'+
            '<i class="fa fa-cloud-download" aria-hidden="true"></i> '+title+'</a> ';
        if (! pageIsPublicFileOrFolder()){
            popupTxt = popupTxt + '<a class="publink" type="track" name="'+title+'" '+
                       'href="publink?filepath='+encodeURI(subfo+
                       '/'+title)+'&user='+encodeURI(gpxpod.username)+'" target="_blank" title="'+
                       escapeHTML(t('gpxpod','This public link will work only if "{title}'+
                       '" or one of its parent folder is '+
                       'shared in "files" app by public link without password', {title:title}))+
                       '">'+
                       '<i class="fa fa-share-alt" aria-hidden="true"></i>'+
                       '</a>';
        }
        popupTxt = popupTxt + '</h3>';
        if (a.length >= TRACKNAMELIST+1){
            popupTxt = popupTxt + '<ul class="trackNamesList">';
            for (var z=0; z<a[TRACKNAMELIST].length; z++){
                var trname = a[TRACKNAMELIST][z];
                if (trname === ''){
                    trname = 'unnamed';
                }
                popupTxt = popupTxt + '<li>'+trname+'</li>';
            }
            popupTxt = popupTxt + '</ul>';
        }

        popupTxt = popupTxt +'<table class="popuptable">';
        popupTxt = popupTxt +'<tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-arrows-h" aria-hidden="true"></i> <b>'+
            t('gpxpod','Distance')+'</b></td>';
        if (a[TOTAL_DISTANCE] !== null){
            if (a[TOTAL_DISTANCE] > 1000){
                popupTxt = popupTxt +'<td> '+
                           (a[TOTAL_DISTANCE]/1000).toFixed(2)+' km</td>';
            }
            else{
                popupTxt = popupTxt +'<td> '+
                           a[TOTAL_DISTANCE].toFixed(2)+' m</td>';
            }
        }
        else{
            popupTxt = popupTxt +'<td> NA</td>';
        }
        popupTxt = popupTxt +'</tr><tr>';

        popupTxt = popupTxt +'<td><i class="fa fa-clock-o" aria-hidden="true"></i> '+
            t('gpxpod','Duration')+' </td><td> '+a[TOTAL_DURATION]+'</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-clock-o" aria-hidden="true"></i> <b>'+
            t('gpxpod','Moving time')+'</b> </td><td> '+a[MOVING_TIME]+'</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-clock-o" aria-hidden="true"></i> '+
            t('gpxpod','Pause time')+' </td><td> '+a[STOPPED_TIME]+'</td>';
        popupTxt = popupTxt +'</tr><tr>';

        var dbs = "no date";
        var dbes = "no date";
        try{
            if (a[DATE_BEGIN] !== '' && a[DATE_BEGIN] !== 'None'){
                var db = moment(a[DATE_BEGIN].replace(' ','T')+'Z');
                db.tz(chosentz);
                var dbs = db.format('YYYY-MM-DD HH:mm:ss (Z)');
            }
            if (a[DATE_END] !== '' && a[DATE_END] !== 'None'){
                var dbe = moment(a[DATE_END].replace(' ','T')+'Z');
                dbe.tz(chosentz);
                var dbes = dbe.format('YYYY-MM-DD HH:mm:ss (Z)');
            }
        }
        catch(err){
        }
        popupTxt = popupTxt +'<td><i class="fa fa-calendar" aria-hidden="true"></i> '+
            t('gpxpod','Begin')+' </td><td> '+dbs+'</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-calendar" aria-hidden="true"></i> '+
            t('gpxpod','End')+' </td><td> '+dbes+'</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-line-chart" aria-hidden="true"></i> <b>'+
            t('gpxpod','Cumulative elevation gain')+'</b> </td><td> '+
            a[POSITIVE_ELEVATION_GAIN]+' m</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-line-chart" aria-hidden="true"></i> '+
            t('gpxpod','Cumulative elevation loss')+' </td><td> '+
                   a[NEGATIVE_ELEVATION_GAIN]+' m</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-line-chart" aria-hidden="true"></i> '+
            t('gpxpod','Minimum elevation')+' </td><td> '+
            a[MIN_ELEVATION]+' m</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-line-chart" aria-hidden="true"></i> '+
            t('gpxpod','Maximum elevation')+' </td><td> '+
            a[MAX_ELEVATION]+' m</td>';
        popupTxt = popupTxt +'</tr><tr>';
        popupTxt = popupTxt +'<td><i class="fa fa-dashboard" aria-hidden="true"></i> <b>'+
            t('gpxpod','Maximum speed')+'</b> </td><td> ';
        if (a[MAX_SPEED] !== null){
            popupTxt = popupTxt+a[MAX_SPEED].toFixed(2)+' km/h';
        }
        else{
            popupTxt = popupTxt +'NA';
        }
        popupTxt = popupTxt +'</td>';
        popupTxt = popupTxt +'</tr><tr>';

        popupTxt = popupTxt +'<td><i class="fa fa-dashboard" aria-hidden="true"></i> '+
            t('gpxpod','Average speed')+' </td><td> ';
        if (a[AVERAGE_SPEED] !== null){
            popupTxt = popupTxt + a[AVERAGE_SPEED].toFixed(2)+' km/h';
        }
        else{
            popupTxt = popupTxt +'NA';
        }
        popupTxt = popupTxt +'</td>';
        popupTxt = popupTxt +'</tr><tr>';

        popupTxt = popupTxt +'<td><i class="fa fa-dashboard" aria-hidden="true"></i> <b>'+
            t('gpxpod','Moving average speed')+'</b> </td><td> ';
        if (a[MOVING_AVERAGE_SPEED] !== null){
            popupTxt = popupTxt + a[MOVING_AVERAGE_SPEED].toFixed(2)+' km/h';
        }
        else{
            popupTxt = popupTxt +'NA';
        }
        popupTxt = popupTxt +'</td></tr>';
        popupTxt = popupTxt + '</table>';

        gpxpod.markersPopupTxt[title] = {};
        gpxpod.markersPopupTxt[title].popup = popupTxt;
    }
}

function removeElevation(){
    // clean other elevation
    if (gpxpod.elevationLayer !== null){
        gpxpod.map.removeControl(gpxpod.elevationLayer);
        delete gpxpod.elevationLayer;
        gpxpod.elevationLayer = null;
        delete gpxpod.elevationTrack;
        gpxpod.elevationTrack = null;
    }
}

function compareSelectedTracks(){
    // build url list
    var params = [];
    var i = 1;
    var param = 'subfolder='+gpxpod.subfolder;
    params.push(param);
    $('#gpxtable tbody input[type=checkbox]:checked').each(function(){
        var aa = $(this).parent().parent().find('td.trackname a.tracklink');
        var trackname = aa.text();
        params.push('name'+i+'='+trackname);
        i++;
    });

    // go to new gpxcomp tab
    var win = window.open(
            gpxpod.gpxcompRootUrl+'?'+params.join('&'), '_blank'
    );
    if(win){
        //Browser has allowed it to be opened
        win.focus();
    }else{
        //Broswer has blocked it
        alert('Allow popups for this site in order to open comparison'+
               ' tab/window.');
    }
}

/*
 * get key events
 */
function checkKey(e){
    e = e || window.event;
    var kc = e.keyCode;
    console.log(kc);

    if (kc === 161 || kc === 223){
        e.preventDefault();
        gpxpod.minimapControl._toggleDisplayButtonClicked();
    }
    if (kc === 60 || kc === 220){
        e.preventDefault();
        $('#sidebar').toggleClass('collapsed');
    }
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) 
        {
            return sParameterName[1];
        }
    }
}

function displayOnHover(tr){
    if (gpxpod.currentAjax !== null){
        gpxpod.currentAjax.abort();
        hideLoadingAnimation();
    }
    if (!tr.find('.drawtrack').is(':checked')){
        var tid = tr.find('.drawtrack').attr('id');

        // use the geojson cache if this track has already been loaded
        var cacheKey = gpxpod.subfolder+'.'+tid;
        if (gpxpod.gpxCache.hasOwnProperty(cacheKey)){
            addHoverTrackDraw(gpxpod.gpxCache[cacheKey], tid);
        }
        // otherwise load it in ajax
        else{
            var req = {
                title : tid,
            }
            // if this is a public folder link page
            if (pageIsPublicFolder()){
                req.username = gpxpod.username;
                req.folder = $('#publicdir').text();
                var url = OC.generateUrl('/apps/gpxpod/getpublicgpx');
            }
            else{
                req.folder = gpxpod.subfolder;
                var url = OC.generateUrl('/apps/gpxpod/getgpx');
            }
            showLoadingAnimation();
            gpxpod.currentAjax = $.post(url, req).done(function (response) {
                gpxpod.gpxCache[cacheKey] = response.content;
                addHoverTrackDraw(response.content, tid);
                hideLoadingAnimation();
            });
        }
    }
}

function addHoverTrackDraw(gpx, tid){
    deleteOnHover();

    if (gpxpod.insideTr){
        var gpxx;
        //if (pageIsPublicFolder()){
        //    var json = geojson;
        //}
        //else{
            gpxx = $(gpx);
        //}
        //alert(gpxx.find('trk').length);

        var lineBorder = $('#linebordercheck').is(':checked');
        var whatToDraw = $('#trackwaypointdisplayselect').val();
        var weight = parseInt($('#lineweight').val());
        var waypointStyle = getWaypointStyle();
        var tooltipStyle = getTooltipStyle();
        var symbolOverwrite = getSymbolOverwrite();


        gpxpod.currentHoverLayer = new L.layerGroup();

        if (whatToDraw !== 't'){
            gpxx.find('>wpt').each(function(){
                var lat = $(this).attr('lat');
                var lon = $(this).attr('lon');
                var name = $(this).find('name').text();
                var cmt = $(this).find('cmt').text();
                var desc = $(this).find('desc').text();
                var sym = $(this).find('sym').text();
                var ele = $(this).find('ele').text();
                var time = $(this).find('time').text();

                var mm = L.marker([lat, lon], {
                    icon: symbolIcons[waypointStyle]
                });
                if (tooltipStyle === 'p'){
                    mm.bindTooltip(brify(name, 20), {permanent: true, className: 'tooltipblue'});
                }
                else{
                    mm.bindTooltip(brify(name, 20), {className: 'tooltipblue'});
                }
                if (symbolOverwrite && sym){
                    if (symbolIcons.hasOwnProperty(sym)){
                        mm.setIcon(symbolIcons[sym]);
                    }
                    else{
                        mm.setIcon(L.divIcon({
                            className: 'unknown',
                            iconAnchor: [12, 12]
                        }));
                    }
                }
                gpxpod.currentHoverLayer.addLayer(mm);
            });
        }

        if (whatToDraw !== 'w'){
            gpxx.find('>trk').each(function(){
                var name = $(this).find('>name').text();
                var cmt = $(this).find('>cmt').text();
                var desc = $(this).find('>desc').text();
                $(this).find('trkseg').each(function(){
                    var latlngs = [];
                    $(this).find('trkpt').each(function(){
                        var lat = $(this).attr('lat');
                        var lon = $(this).attr('lon');
                        latlngs.push([lat,lon]);
                    });
                    var l = L.polyline(latlngs,{
                        weight: weight,
                        style: {color: 'blue', opacity: 1},
                    });
                    if (lineBorder){
                        gpxpod.currentHoverLayerOutlines.addLayer(L.polyline(
                            latlngs,
                            {opacity:1, weight: parseInt(weight*1.6), color:'black'}
                        ));
                    }
                    var tooltipText = tid;
                    if (tid !== name){
                        tooltipText = tooltipText+'<br/>'+name;
                    }
                    if (tooltipStyle === 'p'){
                        l.bindTooltip(tooltipText, {permanent:true, className: 'tooltipblue'});
                    }
                    gpxpod.currentHoverLayer.addLayer(l);
                });
            });

            gpxx.find('>rte').each(function(){
                var latlngs = [];
                var name = $(this).find('>name').text();
                var cmt = $(this).find('>cmt').text();
                var desc = $(this).find('>desc').text();
                $(this).find('rtept').each(function(){
                    var lat = $(this).attr('lat');
                    var lon = $(this).attr('lon');
                    latlngs.push([lat,lon]);
                });
                var l = L.polyline(latlngs,{
                    weight: weight,
                    style: {color: 'blue', opacity: 1},
                });

                if (lineBorder){
                    gpxpod.currentHoverLayerOutlines.addLayer(L.polyline(
                        latlngs,
                        {opacity:1, weight: parseInt(weight*1.6), color:'black'}
                    ));
                }
                var tooltipText = tid;
                if (tid !== name){
                    tooltipText = tooltipText+'<br/>'+name;
                }
                if (tooltipStyle === 'p'){
                    l.bindTooltip(tooltipText, {permanent:true, className: 'tooltipblue'});
                }
                gpxpod.currentHoverLayer.addLayer(l);
            });
        }

        gpxpod.currentHoverLayerOutlines.addTo(gpxpod.map);
        gpxpod.currentHoverLayer.addTo(gpxpod.map);
    }
}

function deleteOnHover(){
    gpxpod.map.removeLayer(gpxpod.currentHoverLayerOutlines);
    gpxpod.currentHoverLayerOutlines.clearLayers();
    if (gpxpod.currentHoverLayer !== null){
        gpxpod.map.removeLayer(gpxpod.currentHoverLayer);
    }
}

function showLoadingMarkersAnimation(){
    //$('div#logo').addClass('spinning');
    $('#loadingmarkers').show();
}

function hideLoadingMarkersAnimation(){
    //$('div#logo').removeClass('spinning');
    $('#loadingmarkers').hide();
}

function showLoadingAnimation(){
    //$('div#logo').addClass('spinning');
    $('#loading').show();
}

function hideLoadingAnimation(){
    //$('div#logo').removeClass('spinning');
    $('#loading').hide();
}

function showDeletingAnimation(){
    $('#deleting').show();
}

function hideDeletingAnimation(){
    $('#deleting').hide();
}

/*
 * the directory selection has been changed
 * @param async : determines if the track load
 * will be done asynchronously or not
 */
function chooseDirSubmit(async){
    // in all cases, we clean the view (marker clusters, table)
    $('#gpxlist').html('');
    removeMarkers();
    removePictures();

    gpxpod.subfolder = $('#subfolderselect').val();
    var sel = $('#subfolderselect').prop('selectedIndex');
    if(sel === 0){
        $('label[for=subfolderselect]').html(t('gpxpod','Folder')+' :');
        return false;
    }
    // we put the public link to folder
    $('label[for=subfolderselect]').html(
        t('gpxpod','Folder')+' : <a class="permalink publink" type="folder" '+
        'name="'+gpxpod.subfolder+'" target="_blank" href="'+
        'pubdirlink?dirpath='+encodeURI(gpxpod.subfolder)+'&user='+encodeURI(gpxpod.username)+'" '+
        'title="'+
        escapeHTML(t('gpxpod', 'Public link to "{folder}" which will work only'+
        ' if this folder is shared in "files" app by public link without password', {folder: gpxpod.subfolder}))+'."'+
        '><i class="fa fa-share-alt" aria-hidden="true"></i></a> '
    );

    var scantype = $('#processtypeselect').val();
    if (scantype === 'all'){
        clearCache();
    }
    gpxpod.map.closePopup();
    // get markers by ajax
    var req = {
        subfolder : gpxpod.subfolder,
        scantype : scantype,
    }
    var url = OC.generateUrl('/apps/gpxpod/getmarkers');
    showLoadingMarkersAnimation();
    gpxpod.currentMarkerAjax = $.ajax({
        type:'POST',
        url:url,
        data:req,
        async:async
    }).done(function (response) {
        getAjaxMarkersSuccess(response.markers, response.python_output);
        getAjaxPicturesSuccess(response.pictures);
    }).always(function(){
        hideLoadingMarkersAnimation();
        gpxpod.currentMarkerAjax = null;
    });
}

function removePictures(){
    for (var i=0; i<gpxpod.picturePopups.length; i++){
        gpxpod.map.closePopup(gpxpod.picturePopups[i]);
        delete gpxpod.picturePopups[i];
    }
    gpxpod.picturePopups = [];

    for (var i=0; i<gpxpod.pictureSmallMarkers.length; i++){
        gpxpod.pictureSmallMarkers[i].remove();
        delete gpxpod.pictureSmallMarkers[i];
    }
    gpxpod.pictureSmallMarkers = [];

    for (var i=0; i<gpxpod.pictureBigMarkers.length; i++){
        gpxpod.pictureBigMarkers[i].remove();
        delete gpxpod.pictureBigMarkers[i];
    }
    gpxpod.pictureBigMarkers = [];
}

function getAjaxPicturesSuccess(pictures){
    var piclist = $.parseJSON(pictures);
    if (Object.keys(piclist).length > 0){
        $('#showpicsdiv').show();
    }
    else{
        $('#showpicsdiv').hide();
    }

    var picstyle = $('#picturestyleselect').val();
    var smallPreviewX = 80;
    var smallPreviewY = 80;
    var bigPreviewX = 200;
    var bigPreviewY = 200;

    // pictures work in normal page and public dir page
    // but the preview and DL urls are different
    if (pageIsPublicFolder()){
        var tokenspl = gpxpod.token.split('?');
        var token = tokenspl[0];
        if (tokenspl.length === 1){
            var subpath = '/';
        }
        else{
            var subpath = tokenspl[1].replace('path=', '');
        }
        var smallPreviewParams = {
            file: '',
            x:smallPreviewX,
            y:smallPreviewY,
            t: token
        }
        var bigPreviewParams = {
            file: '',
            x:bigPreviewX,
            y:bigPreviewY,
            t: token
        }
        var previewUrl = OC.generateUrl('/apps/files_sharing/ajax/publicpreview.php?');

        var dlParams = {
            path: subpath,
            files: ''
        }
        var dlUrl = OC.generateUrl('/s/'+token+'/download?');
    }
    else{
        var dlParams = {
            dir: gpxpod.subfolder,
            files:''
        }
        var dlUrl = OC.generateUrl('/apps/files/ajax/download.php?');
        var smallPreviewParams = {
            x: smallPreviewX,
            y: smallPreviewY,
            forceIcon: 0,
            file: ''
        };
        var bigPreviewParams = {
            x: bigPreviewX,
            y: bigPreviewY,
            forceIcon: 0,
            file: ''
        };
        var previewUrl = OC.generateUrl('/core/preview.png?');
        var subpath = gpxpod.subfolder;
    }

    for (var p in piclist){
        dlParams.files = p;
        var durl = dlUrl + $.param(dlParams);
        smallPreviewParams.file = subpath + '/' + p;
        bigPreviewParams.file = subpath + '/' + p;
        var smallpurl = previewUrl + $.param(smallPreviewParams);
        var bigpurl = previewUrl + $.param(bigPreviewParams);

        // POPUP
        var previewDiv = '<div class="popupImage" style="background-image:url('+smallpurl+'); background-size: 80px auto;"></div>';
        var popupContent = '<a class="group1" href="'+durl+'" title="'+p+'">'+
            previewDiv+'</a><a href="'+durl+'" target="_blank">'+
            '<i class="fa fa-cloud-download" aria-hidden="true"></i> '+
            t('gpxpod','download')+'</a>';

        var popup = L.popup({
            autoClose: false,
            //offset: L.point(0, -30),
            autoPan: false,
            closeOnClick: false
        });
        popup.setContent(popupContent);
        popup.setLatLng(L.latLng(piclist[p][0], piclist[p][1]));
        gpxpod.picturePopups.push(popup);

        // MARKERS
        var tooltipContent = p+'<br/><img src="'+bigpurl+'"/>';
        var bm = L.marker(L.latLng(piclist[p][0], piclist[p][1]),
            {
                icon: L.divIcon({
                    className: 'leaflet-marker-red',
                    iconAnchor: [12, 41]
                })
            }
        );
        var sm = L.marker(L.latLng(piclist[p][0], piclist[p][1]),
            {
                icon: L.divIcon({
                    iconSize:L.point(6,6),
                    className: 'smallRedMarker'
                })
            }
        );

        gpxpod.pictureSmallMarkers.push(sm);
        gpxpod.pictureBigMarkers.push(bm);
        sm.bindTooltip(tooltipContent);
        bm.bindTooltip(tooltipContent);
    }

    if ($('#showpicscheck').is(':checked')){
        showPictures();
    }
}

function hidePictures(){
    for (var i=0; i<gpxpod.picturePopups.length; i++){
        gpxpod.map.closePopup(gpxpod.picturePopups[i]);
    }
    for (var i=0; i<gpxpod.pictureSmallMarkers.length; i++){
        gpxpod.pictureSmallMarkers[i].remove();
    }
    for (var i=0; i<gpxpod.pictureBigMarkers.length; i++){
        gpxpod.pictureBigMarkers[i].remove();
    }
}

function showPictures(){
    var picstyle = $('#picturestyleselect').val();

    if (picstyle === 'p'){
        for (var i=0; i<gpxpod.picturePopups.length; i++){
            //gpxpod.map.openPopup(gpxpod.picturePopups[i]);
            gpxpod.picturePopups[i].openOn(gpxpod.map);
        }
        $(".group1").colorbox({rel:'group1', height:"90%"});
    }
    else if(picstyle === 'sm'){
        for (var i=0; i<gpxpod.pictureSmallMarkers.length; i++){
            gpxpod.pictureSmallMarkers[i].addTo(gpxpod.map);
        }
    }
    else{
        for (var i=0; i<gpxpod.pictureBigMarkers.length; i++){
            gpxpod.pictureBigMarkers[i].addTo(gpxpod.map);
        }
    }
}

function picStyleChange(){
    hidePictures();
    if ($('#showpicscheck').is(':checked')){
        showPictures();
    }
}

function picShowChange(){
    if ($('#showpicscheck').is(':checked')){
        showPictures();
    }
    else{
        hidePictures();
    }
}

function getAjaxMarkersSuccess(markerstxt, python_output){
    // load markers
    loadMarkers(markerstxt);
    // remove all draws
    for(var tid in gpxpod.gpxlayers){
        removeTrackDraw(tid);
    }
    // handle python error
    $('#python_output').html(python_output);
    if ($('#autozoomcheck').is(':checked')){
        zoomOnAllMarkers();
    }
    else{
        gpxpod.map.setView(new L.LatLng(27, 5), 3);
    }
}

function zoomOnAllMarkers(){
    if (gpxpod.markers.length > 0){
        var north = gpxpod.markers[0][LAT];
        var south = gpxpod.markers[0][LAT];
        var east = gpxpod.markers[0][LON];
        var west = gpxpod.markers[0][LON];
        for (var i = 1; i < gpxpod.markers.length; i++) {
            var m = gpxpod.markers[i];
            if (m[LAT] > north){
                north = m[LAT];
            }
            if (m[LAT] < south){
                south = m[LAT];
            }
            if (m[LON] < west){
                west = m[LON];
            }
            if (m[LON] > east){
                east = m[LON];
            }
        }
        gpxpod.map.fitBounds([[south, west],[north, east]],
                {animate:true, paddingTopLeft: [parseInt($('#sidebar').css('width')), 0]}
        );
    }
}

// read in #markers
function loadMarkers(m){
    if (m === ''){
        var markerstxt = $('#markers').text();
    }
    else{
        var markerstxt = m;
    }
    if (markerstxt !== null && markerstxt !== '' && markerstxt !== false){
        gpxpod.markers = $.parseJSON(markerstxt).markers;
        gpxpod.subfolder = $('#subfolderselect').val();
        gpxpod.gpxcompRootUrl = $('#gpxcomprooturl').text();
        genPopupTxt();

    }
    else{
        delete gpxpod.markers;
        gpxpod.markers = [];
        console.log('no marker');
    }
    redraw();
    updateTrackListFromBounds();
}

function stopGetMarkers(){
    if (gpxpod.currentMarkerAjax !== null){
        // abort ajax
        gpxpod.currentMarkerAjax.abort();
        gpxpod.currentMarkerAjax = null;
    }
}

/*
 * If timezone changes, we regenerate popups
 * by reloading current folder
 */
function tzChanged(){
    $('#processtypeselect').val('new');
    $('#subfolderselect').change();

    // if it's a public link, we display it again to update dates
    if (pageIsPublicFolder()){
        displayPublicDir();
    }
    else if (pageIsPublicFile()){
        displayPublicTrack();
    }
}

function pageIsPublicFile(){
    var publicgpx = $('p#publicgpx').html();
    var publicdir = $('p#publicdir').html();
    return (publicgpx !== '' && publicdir === '');
}
function pageIsPublicFolder(){
    var publicgpx = $('p#publicgpx').html();
    var publicdir = $('p#publicdir').html();
    return (publicgpx === '' && publicdir !== '');
}
function pageIsPublicFileOrFolder(){
    var publicgpx = $('p#publicgpx').html();
    var publicdir = $('p#publicdir').html();
    return (publicgpx !== '' || publicdir !== '');
}

function displayPublicDir(){
    $('p#nofolder').hide();
    $('p#nofoldertext').hide();
    $('div#folderdiv').hide();
    $('div#scantypediv').hide();
    $('p#nofolder').hide();
    var publicdir = $('p#publicdir').html();

    var url = OC.generateUrl('/s/'+gpxpod.token);
    if ($('#pubtitle').length == 0){
        $('div#logofolder').append(
                '<p id="pubtitle" style="text-align:center; font-size:14px;">'+
                '<br/>'+t('gpxpod','Public folder share')+' :<br/>'+
                '<a href="'+url+'" class="toplink" title="'+t('gpxpod','download')+'"'+
                ' target="_blank">'+publicdir+'</a>'+
                '</p>'
        );
    }

    var publicmarker = $('p#publicmarker').html();
    var markers = $.parseJSON(publicmarker);
    gpxpod.markers = markers['markers'];

    genPopupTxt();
    addMarkers();
    updateTrackListFromBounds();
    if ($('#autozoomcheck').is(':checked')){
        zoomOnAllMarkers();
    }
    else{
        gpxpod.map.setView(new L.LatLng(27, 5), 3);
    }

    var pictures = $('p#pictures').html();
    getAjaxPicturesSuccess(pictures);
}

/*
 * manage display of public track
 * hide folder selection
 * get marker content, generate popup
 * create a markercluster
 * and finally draw the track
 */
function displayPublicTrack(){
    $('p#nofolder').hide();
    $('p#nofoldertext').hide();
    $('div#folderdiv').hide();
    $('div#scantypediv').hide();
    removeMarkers();
    gpxpod.map.closePopup();

    var publicgpx = $('p#publicgpx').html();
    publicgpx = $('<div/>').html(publicgpx).text();
    var publicmarker = $('p#publicmarker').html();
    var a = $.parseJSON(publicmarker);
    gpxpod.markers = [a];
    genPopupTxt();

    var markerclu = L.markerClusterGroup({ chunkedLoading: true });
    var title = a[NAME];
    var url = OC.generateUrl('/s/'+gpxpod.token);
    if ($('#pubtitle').length == 0){
        $('div#logofolder').append(
                '<p id="pubtitle" style="text-align:center; font-size:14px;">'+
                '<br/>'+t('gpxpod','Public file share')+' :<br/>'+
                '<a href="'+url+'" class="toplink" title="'+t('gpxpod','download')+'"'+
                ' target="_blank">'+title+'</a>'+
                '</p>'
        );
    }
    var marker = L.marker(L.latLng(a[LAT], a[LON]), { title: title });
    marker.bindPopup(
            gpxpod.markersPopupTxt[title].popup,
            {
                autoPan:true,
                autoClose: true,
                closeOnClick: true
            }
            );
    gpxpod.markersPopupTxt[title].marker = marker;
    markerclu.addLayer(marker);
    if ($('#displayclusters').is(':checked')){
        gpxpod.map.addLayer(markerclu);
    }
    gpxpod.markerLayer = markerclu;
    if ($('#colorcriteria').val() !== 'none'){
        addColoredTrackDraw(publicgpx, title, true);
    }
    else{
        removeTrackDraw(title);
        addTrackDraw(publicgpx, title, true);
    }
}

/*
 * send ajax request to clean .marker,
 * .geojson and .geojson.colored files
 */
function askForClean(forwhat){
    // ask to clean by ajax
    var req = {
        forall : forwhat
    }
    var url = OC.generateUrl('/apps/gpxpod/cleanMarkersAndGeojsons');
    showDeletingAnimation();
    $('#clean_results').html('');
    $('#python_output').html('');
    $.ajax({
        type:'POST',
        url:url,
        data:req,
        async:true
    }).done(function (response) {
        $('#clean_results').html(
                'Those files were deleted :\n<br/>'+
                response.deleted+'\n<br/>'+
                'Problems :\n<br/>'+response.problems
                );
    }).always(function(){
        hideDeletingAnimation();
    });
}

function addTileServer(){
    var sname = $('#tileservername').val();
    var surl = $('#tileserverurl').val();
    if (sname === '' || surl === ''){
        alert('Server name or server url should not be empty');
        return;
    }
    $('#tileservername').val('');
    $('#tileserverurl').val('');

    var req = {
        servername : sname,
        serverurl : surl
    }
    var url = OC.generateUrl('/apps/gpxpod/addTileServer');
    $.ajax({
        type:'POST',
        url:url,
        data:req,
        async:true
    }).done(function (response) {
        //alert(response.done);
        if (response.done){
            $('#tileserverlist ul').prepend(
                '<li name="'+sname+'" title="'+surl+'">'+sname+' <button>'+
                '<i class="fa fa-trash" aria-hidden="true" style="color:red;"></i> '+
                t('gpxpod','Delete')+'</button></li>'
            );
            // add tile server in leaflet control
            var newlayer = new L.TileLayer(surl,
                    {maxZoom: 18, attribution: 'custom tile server'});
            gpxpod.activeLayers.addBaseLayer(newlayer, sname);
            gpxpod.baseLayers[sname] = newlayer;
        }
    }).always(function(){
    });
}

function deleteTileServer(li){
    var sname = li.attr('name');
    var req = {
        servername : sname
    }
    var url = OC.generateUrl('/apps/gpxpod/deleteTileServer');
    $.ajax({
        type:'POST',
        url:url,
        data:req,
        async:true
    }).done(function (response) {
        //alert(response.done);
        if (response.done){
            li.remove();
            var activeLayerName = gpxpod.activeLayers.getActiveBaseLayer().name;
            // if we delete the active layer, first select another
            if (activeLayerName === sname){
                $('input.leaflet-control-layers-selector').first().click();
            }
            gpxpod.activeLayers.removeLayer(gpxpod.baseLayers[sname]);
            delete gpxpod.baseLayers[sname];
        }
    }).always(function(){
    });
}

function correctElevation(link){
    var track = link.attr('track');
    var folder = gpxpod.subfolder;
    var smooth = (link.attr('class') == 'csrtms');
    showLoadingAnimation();
    var req = {
        trackname: track,
        folder: folder,
        smooth: smooth
    }
    var url = OC.generateUrl('/apps/gpxpod/processTrackElevations');
    $.ajax({
        type:'POST',
        url:url,
        data:req,
        async:true
    }).done(function (response) {
        // erase track cache to be sure it will be reloaded
        delete gpxpod.gpxCache[folder+'.'+track];
        // processed successfully, we reload folder
        $('#processtypeselect').val('new');
        $('#subfolderselect').change();
    }).always(function(){
        hideLoadingAnimation();
    });
}

/*
 * returns true if at least one point of the track is
 * inside the map bounds
 */
function trackCrossesMapBounds(shortPointList, mapb){
    if (typeof shortPointList !== 'undefined'){
        for (var i = 0; i < shortPointList.length; i++) {
            var p = shortPointList[i];
            if (mapb.contains(new L.LatLng(p[0], p[1]))){
                return true;
            }
        }
    }
    return false;
}

function brify(str, linesize){
    var res = '';
    var words = str.split(' ');
    var cpt = 0;
    var toAdd = '';
    for (var i=0; i<words.length; i++){
        if ((cpt + words[i].length) < linesize){
            toAdd += words[i]+' ';
            cpt += words[i].length + 1;
        }
        else{
            res += toAdd + '<br/>';
            toAdd = words[i]+' ';
            cpt = words[i].length + 1;
        }
    }
    res += toAdd;
    return res;
}

function getWaypointStyle(){
    return $('#waypointstyleselect').val();
}

function getTooltipStyle(){
    return $('#tooltipstyleselect').val();
}

function getSymbolOverwrite(){
    return $('#symboloverwrite').is(':checked');
}

function restoreOptions(){
    var url = OC.generateUrl('/apps/gpxpod/getOptionsValues');
    var req = {
    }
    var optionsValues = '{}';
    $.ajax({
        type: 'POST',
        url: url,
        data: req,
        async: false
    }).done(function (response) {
        optionsValues = response.values;
        //alert('option values : '+optionsValues);
    }).fail(function(){
        alert('failed to restore options values');
    });
    optionsValues = $.parseJSON(optionsValues);
    if (optionsValues.trackwaypointdisplay !== undefined){
        $('#trackwaypointdisplayselect').val(optionsValues.trackwaypointdisplay);
    }
    if (optionsValues.waypointstyle !== undefined &&
        symbolIcons.hasOwnProperty(optionsValues.waypointstyle)){
        $('#waypointstyleselect').val(optionsValues.waypointstyle);
        updateWaypointStyle(optionsValues.waypointstyle);
    }
    if (optionsValues.tooltipstyle !== undefined){
        $('#tooltipstyleselect').val(optionsValues.tooltipstyle);
    }
    if (optionsValues.colorcriteria !== undefined){
        $('#colorcriteria').val(optionsValues.colorcriteria);
    }
    if (optionsValues.tablecriteria !== undefined){
        $('#tablecriteriasel').val(optionsValues.tablecriteria);
    }
    if (optionsValues.picturestyle !== undefined){
        $('#picturestyleselect').val(optionsValues.picturestyle);
    }
    if (optionsValues.lineweight !== undefined){
        $('#lineweight').val(optionsValues.lineweight);
    }
    if (optionsValues.displayclusters !== undefined){
        $('#displayclusters').prop('checked', optionsValues.displayclusters);
    }
    if (optionsValues.openpopup !== undefined){
        $('#openpopupcheck').prop('checked', optionsValues.openpopup);
    }
    if (optionsValues.autozoom !== undefined){
        $('#autozoomcheck').prop('checked', optionsValues.autozoom);
    }
    if (optionsValues.transparent !== undefined){
        $('#transparentcheck').prop('checked', optionsValues.transparent);
    }
    if (optionsValues.updtracklist !== undefined){
        $('#updtracklistcheck').prop('checked', optionsValues.updtracklist);
    }
    if (optionsValues.showpics !== undefined){
        $('#showpicscheck').prop('checked', optionsValues.showpics);
    }
    if (optionsValues.symboloverwrite !== undefined){
        $('#symboloverwrite').prop('checked', optionsValues.symboloverwrite);
    }
    if (optionsValues.lineborder !== undefined){
        $('#linebordercheck').prop('checked', optionsValues.lineborder);
    }
}

function saveOptions(){
    var optionsValues = {};
    optionsValues.trackwaypointdisplay = $('#trackwaypointdisplayselect').val();
    optionsValues.waypointstyle = $('#waypointstyleselect').val();
    optionsValues.tooltipstyle = $('#tooltipstyleselect').val();
    optionsValues.colorcriteria = $('#colorcriteria').val();
    optionsValues.tablecriteria = $('#tablecriteriasel').val();
    optionsValues.picturestyle = $('#picturestyleselect').val();
    optionsValues.displayclusters = $('#displayclusters').is(':checked');
    optionsValues.openpopup = $('#openpopupcheck').is(':checked');
    optionsValues.autozoom = $('#autozoomcheck').is(':checked');
    optionsValues.transparent = $('#transparentcheck').is(':checked');
    optionsValues.updtracklist = $('#updtracklistcheck').is(':checked');
    optionsValues.showpics = $('#showpicscheck').is(':checked');
    optionsValues.symboloverwrite = $('#symboloverwrite').is(':checked');
    optionsValues.lineborder = $('#linebordercheck').is(':checked');
    optionsValues.lineweight = $('#lineweight').val();
    //alert('to save : '+JSON.stringify(optionsValues));

    var req = {
        optionsValues : JSON.stringify(optionsValues),
    }
    var url = OC.generateUrl('/apps/gpxpod/saveOptionsValues');
    $.ajax({
        type: 'POST',
        url: url,
        data: req,
        async: true
    }).done(function (response) {
        //alert(response);
    }).fail(function(){
        alert('failed to save options values');
    });
}

function fillWaypointStyles(){
    for (var st in symbolIcons){
        $('select#waypointstyleselect').append('<option value="'+st+'">'+st+'</option>');
    }
    $('select#waypointstyleselect').val('Pin, Blue');
    updateWaypointStyle('Pin, Blue');
}

function clearCache(){
    var keysToRemove = [];
    for (var k in gpxpod.gpxCache){
        keysToRemove.push(k);
    }

    for(var i=0; i<keysToRemove.length; i++){
        delete gpxpod.gpxCache[keysToRemove[i]];
    }
    gpxpod.gpxCache = {};
}

// if gpxedit_version > one.two.three and we're connected and not on public page
function isGpxeditCompliant(one, two, three){
    var ver = $('p#gpxedit_version').html();
    if (ver !== ''){
        var vspl = ver.split('.');
        return (parseInt(vspl[0]) > one || parseInt(vspl[1]) > two || parseInt(vspl[2]) > three);
    }
    else{
        return false;
    }
}

function addExtraSymbols(){
    var url = OC.generateUrl('/apps/gpxedit/getExtraSymbol?');
    $('ul#extrasymbols li').each(function(){
        var name = $(this).attr('name');
        var smallname = $(this).html();
        var fullurl = url+'name='+encodeURI(name);
        var d = L.icon({
            iconUrl: fullurl,
            iconSize: L.point(24, 24),
            iconAnchor: [12, 12]
        });
        symbolIcons[smallname] = d;
    });
}

function updateWaypointStyle(val){
    var sel = $('#waypointstyleselect');
    sel.removeClass(sel.attr('class'));
    sel.attr('style','');
    if (symbolSelectClasses.hasOwnProperty(val)){
        sel.addClass(symbolSelectClasses[val]);
    }
    else if (val !== ''){
        var url = OC.generateUrl('/apps/gpxedit/getExtraSymbol?');
        var fullurl = url+'name='+encodeURI(val+'.png');
        sel.attr('style',
                'background: url(\''+fullurl+'\') no-repeat '+
                'right 8px center rgba(240, 240, 240, 0.90);'+
                'background-size: contain;');
    }
}

$(document).ready(function(){
    if (isGpxeditCompliant(0, 0, 2)){
        addExtraSymbols();
    }
    fillWaypointStyles();
    if ( !pageIsPublicFileOrFolder() ){
        restoreOptions();
    }

    gpxpod.username = $('p#username').html();
    gpxpod.token = $('p#token').html();
    gpxpod.gpxedit_version = $('p#gpxedit_version').html();
    gpxpod.gpxedit_compliant = isGpxeditCompliant(0, 0, 1);
    gpxpod.gpxedit_url = OC.generateUrl('/apps/gpxedit/?');
    load();
    loadMarkers('');
    if (pageIsPublicFolder()){
        gpxpod.subfolder = $('#publicdir').text();
    }
    $('body').on('change','.drawtrack', function(e) {
        // in publink, no check
        if (pageIsPublicFile()){
            e.preventDefault();
            $(this).prop('checked', true);
            return;
        }
        var tid = $(this).attr('id');
        if ($(this).is(':checked')){
            if (gpxpod.currentAjax !== null){
                gpxpod.currentAjax.abort();
                hideLoadingAnimation();
            }
            if ($('#colorcriteria').val() !== 'none'){
                var cacheKey = gpxpod.subfolder+'.'+tid;
                if (gpxpod.gpxCache.hasOwnProperty(cacheKey)){
                    addColoredTrackDraw(gpxpod.gpxCache[cacheKey], tid, true);
                }
                else{
                    var req = {
                        title : tid,
                    }
                    // are we in the public folder page ?
                    if (pageIsPublicFolder()){
                        req.username = gpxpod.username;
                        req.folder = $('#publicdir').text();
                        var url = OC.generateUrl('/apps/gpxpod/getpublicgpx');
                    }
                    else{
                        req.folder = gpxpod.subfolder;
                        var url = OC.generateUrl('/apps/gpxpod/getgpx');
                    }
                    showLoadingAnimation();
                    $.post(url, req).done(function (response) {
                        gpxpod.gpxCache[cacheKey] = response.track;
                        addColoredTrackDraw(response.content, tid, true);
                        hideLoadingAnimation();
                    });
                }
            }
            else{
                var cacheKey = gpxpod.subfolder+'.'+tid;
                if (gpxpod.gpxCache.hasOwnProperty(cacheKey)){
                    addTrackDraw(gpxpod.gpxCache[cacheKey], tid, true);
                }
                else{
                    var req = {
                        title : tid,
                    }
                    // are we in the public folder page ?
                    if (pageIsPublicFolder()){
                        req.username = gpxpod.username;
                        req.folder = $('#publicdir').text();
                        var url = OC.generateUrl('/apps/gpxpod/getpublicgpx');
                    }
                    else{
                        req.folder = gpxpod.subfolder;
                        var url = OC.generateUrl('/apps/gpxpod/getgpx');
                    }
                    showLoadingAnimation();
                    $.post(url, req).done(function (response) {
                        gpxpod.gpxCache[cacheKey] = response.track;
                        addTrackDraw(response.content, tid, true);
                        hideLoadingAnimation();
                    });
                }
            }
        }
        else{
            removeTrackDraw(tid);
            gpxpod.map.closePopup();
        }
    });
    $('body').on('mouseenter','#gpxtable tbody tr', function() {
        gpxpod.insideTr = true;
        displayOnHover($(this));
        if ($('#transparentcheck').is(':checked')){
            $('#sidebar').addClass('transparent');
        }
    });
    $('body').on('mouseleave','#gpxtable tbody tr', function() {
        gpxpod.insideTr = false;
        $('#sidebar').removeClass('transparent');
        deleteOnHover();
    });
    // keeping table sort order
    $('body').on('sortEnd','#gpxtable', function(sorter) {
        gpxpod.tablesortCol = sorter.target.config.sortList[0];
    });
    $('body').on('change','#transparentcheck', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
    });
    $('body').on('change','#autozoomcheck', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
    });
    $('body').on('change','#openpopupcheck', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
    });
    $('body').on('change','#displayclusters', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        redraw();
    });
    $('body').on('change','#picturestyleselect', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        picStyleChange();
    });
    $('body').on('spinstop','#lineweight', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
    });
    $('body').on('change','#linebordercheck', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
    });
    $('body').on('change','#showpicscheck', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        picShowChange();
    });
    $('body').on('click','#comparebutton', function(e) {
        compareSelectedTracks();
    });
    $('body').on('click','#removeelevation', function(e) {
        removeElevation();
    });
    $('body').on('click','#updtracklistcheck', function(e) {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if ($('#updtracklistcheck').is(':checked')){
            $('#ticv').text('Tracks from current view');
            $('#tablecriteria').show();
        }
        else{
            $('#ticv').text('All tracks');
            $('#tablecriteria').hide();
        }
        updateTrackListFromBounds();
    });
    // in case #updtracklistcheck is restored unchecked
    if (!pageIsPublicFileOrFolder()){
        if ($('#updtracklistcheck').is(':checked')){
            $('#ticv').text('Tracks from current view');
            $('#tablecriteria').show();
        }
        else{
            $('#ticv').text('All tracks');
            $('#tablecriteria').hide();
        }
    }

    $('#tablecriteriasel').change(function(e){
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        updateTrackListFromBounds();
    });
    document.onkeydown = checkKey;

    $('#lineweight').spinner({
        min: 2,
        max: 20,
        step:1,
    })
    // fields in filters sidebar tab
    $('#datemin').datepicker({
        showAnim: 'slideDown',
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
    $('#datemax').datepicker({
        showAnim: 'slideDown',
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
    $('#distmin').spinner({
        min: 0,
        step:500,
    })
    $('#distmax').spinner({
        min: 0,
        step:500,
    })
    $('#cegmin').spinner({
        min: 0,
        step:100,
    })
    $('#cegmax').spinner({
        min: 0,
        step:100,
    })
    $('#clearfilter').click(function(e){
        e.preventDefault();
        clearFiltersValues();
        redraw();
        updateTrackListFromBounds();

    });
    $('#applyfilter').click(function(e){
        e.preventDefault();
        redraw();
        updateTrackListFromBounds();
    });
    $('form[name=choosedir]').submit(function(e){
        e.preventDefault();
        chooseDirSubmit(true);
    });
    $('select#subfolderselect').change(function(e){
        stopGetMarkers();
        chooseDirSubmit(true);
    });

    // loading gifs
    //$('#loadingmarkers').css('background-image', 'url('+ OC.imagePath('core', 'loading.gif') + ')');
    //$('#loading').css('background-image', 'url('+ OC.imagePath('core', 'loading.gif') + ')');
    //$('#deleting').css('background-image', 'url('+ OC.imagePath('core', 'loading.gif') + ')');

    // TIMEZONE
    var mytz = jstz.determine_timezone();
    var mytzname = mytz.timezone.olson_tz;
    var tzoptions = '';
    for (var tzk in jstz.olson.timezones){
        var tz = jstz.olson.timezones[tzk];
        tzoptions = tzoptions + '<option value="'+tz.olson_tz
            +'">'+tz.olson_tz+' (GMT'+tz.utc_offset+')</option>\n';
    }
    $('#tzselect').html(tzoptions);
    $('#tzselect').val(mytzname);
    $('#tzselect').change(function(e){
        tzChanged();
    });
    tzChanged();

    $('#clean').click(function(e){
        e.preventDefault();
        askForClean("nono");
    });
    $('#cleanall').click(function(e){
        e.preventDefault();
        askForClean("all");
    });

    // Custom tile server management
    $('body').on('click','#tileserverlist button', function(e) {
        deleteTileServer($(this).parent());
    });
    $('#addtileserver').click(function(){
        addTileServer();
    });

    // elevation correction of one track
    $('body').on('click','.csrtm', function(e) {
        correctElevation($(this));
    });
    $('body').on('click','.csrtms', function(e) {
        correctElevation($(this));
    });

    // change coloring makes public track (publink) being redrawn
    $('#colorcriteria').change(function(e){
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
    });
    $('#waypointstyleselect').change(function(e){
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
        updateWaypointStyle($(this).val());
    });
    $('#tooltipstyleselect').change(function(e){
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
    });
    $('body').on('change','#symboloverwrite', function() {
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
    });
    $('#trackwaypointdisplayselect').change(function(e){
        if (!pageIsPublicFileOrFolder()){
            saveOptions();
        }
        if (pageIsPublicFile()){
            displayPublicTrack();
        }
    });

    // in public link and public folder link :
    // hide compare button and custom tiles server management
    if (pageIsPublicFileOrFolder()){
        $('button#comparebutton').hide();
        $('div#tileserverlist').hide();
        $('div#tileserveradd').hide();
    }

    $('body').on('click','a.publink', function(e) {
        var subfo = gpxpod.subfolder;
        if (subfo === '/'){
            subfo = '';
        }
        e.preventDefault();
        var autopopup = '&autopopup=yes';
        if (! $('#openpopupcheck').is(':checked')){
            autopopup = '&autopopup=no';
        }
        var autozoom = '&autozoom=yes';
        if (! $('#autozoomcheck').is(':checked')){
            autozoom = '&autozoom=no';
        }
        var tableutd = '&tableutd=yes';
        if (! $('#updtracklistcheck').is(':checked')){
            tableutd = '&tableutd=no';
        }
        var activeLayerName = gpxpod.activeLayers.getActiveBaseLayer().name;
        var layerparam = '&layer='+encodeURI(activeLayerName);
        var link = $(this).attr('href');
        var url = OC.generateUrl('/apps/gpxpod/'+link);
        url = window.location.origin + url;

        var name = $(this).attr('name');
        var type = $(this).attr('type');
        var ttype = t('gpxpod', $(this).attr('type'));
        var title = t('gpxpod', 'Public link to')+' '+ttype+' : '+name;
        if (type === 'track'){

            var ajaxurl = OC.generateUrl('/apps/gpxpod/isFileShareable');
            var req = {
                trackpath: subfo+'/'+name,
                username: gpxpod.username
            }
            var isShareable;
            $.ajax({
                type:'POST',
                url:ajaxurl,
                data:req,
                async:false
            }).done(function (response) {
                isShareable = response.response;
            });

            var txt;
            if (isShareable){
                txt = '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
            }
            else{
                txt = '<i class="fa fa-times-circle" style="color:red;" aria-hidden="true"></i> ';
                txt = txt + t('gpxpod','This public link will work only if "{title}'+
                        '" or one of its parent folder is '+
                        'shared in "files" app by public link without password', {title: name});
            }


            $('#linkinput').val(url+layerparam);
        }
        else{
            var folder = $(this).attr('name');

            var ajaxurl = OC.generateUrl('/apps/gpxpod/isFolderShareable');
            var req = {
                folderpath: gpxpod.subfolder,
                username: gpxpod.username
            }
            var isShareable;
            $.ajax({
                type:'POST',
                url:ajaxurl,
                data:req,
                async:false
            }).done(function (response) {
                isShareable = response.response;
            });

            var txt;
            if (isShareable){
                txt = '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
            }
            else{
                txt = '<i class="fa fa-times-circle" style="color:red;" aria-hidden="true"></i> ';
                txt = txt + t('gpxpod', 'Public link to "{folder}" which will work only'+
                        ' if this folder is shared in "files" app by public link without password', {folder: name});
            }

            $('#linkinput').val(url+autozoom+autopopup+tableutd+layerparam);
        }
        $('#linklabel').html(txt);
        $('#linkdialog').dialog({
            title: title,
            closeText: 'show',
            width: 400
        });
        $('#linkinput').select();
    });

    $('body').on('click','h3#optiontitle', function(e) {
        if ($('#optionscontent').is(':visible')){
            $('#optionscontent').slideUp();
            $('#optiontoggle').html('<i class="fa fa-expand"></i>');
        }
        else{
            $('#optionscontent').slideDown();
            $('#optiontoggle').html('<i class="fa fa-compress"></i>');
        }
    });

    // on public pages : load checkboxes states from GET params
    if (pageIsPublicFolder()){
        var autopopup = getUrlParameter('autopopup');
        if (typeof autopopup !== 'undefined' && autopopup === 'no'){
            $('#openpopupcheck').prop('checked', false);
        }
        else{
            $('#openpopupcheck').prop('checked', true);
        }
        var autozoom = getUrlParameter('autozoom');
        if (typeof autozoom !== 'undefined' && autozoom === 'no'){
            $('#autozoomcheck').prop('checked', false);
        }
        else{
            $('#autozoomcheck').prop('checked', true);
        }
        var tableutd = getUrlParameter('tableutd');
        if (typeof tableutd !== 'undefined' && tableutd === 'no'){
            $('#updtracklistcheck').prop('checked', false);
        }
        else{
            $('#updtracklistcheck').prop('checked', true);
        }
    }

    $('body').on('click','.comtext', function(e) {
        $(this).slideUp();
    });
    $('body').on('click','.combutton', function(e) {
        var fid = $(this).attr('combutforfeat');
        var p = $('p[comforfeat="'+fid+'"]');
        if (p.is(':visible')){
            p.slideUp();
        }
        else{
            p.slideDown();
        }
    });
    $('body').on('click','.desctext', function(e) {
        $(this).slideUp();
    });
    $('body').on('click','.descbutton', function(e) {
        var fid = $(this).attr('descbutforfeat');
        var p = $('p[descforfeat="'+fid+'"]');
        if (p.is(':visible')){
            p.slideUp();
        }
        else{
            p.slideDown();
        }
    });

});

})(jQuery, OC);
